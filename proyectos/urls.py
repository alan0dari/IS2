from django.conf.urls import url
from django.urls import re_path,include
from proyectos import views
from proyectos.views import CambiarFase, horasTrabajadas, GeneratePDFhorasTrabajadas, GeneratePDFDesempenho
from proyectos.views import HistorialProyecto

app_name = "proyectos"


urlpatterns= [
    # /proyectos/123456.../
    re_path(r"^(?P<pk>\d+)/$", views.detalle, name="detalle"),
    # /proyectos/registrar/
    re_path(r"^registrar/$", views.adicion, name="registrar"),
    # /proyectos/123456.../editar/
    re_path(r"^(?P<pk>\d+)/editar/$", views.modificacion, name="editar"),
    # /proyectos/
    re_path(r"^(?P<pk>\d+)/eliminar/$", views.baja, name="eliminar"),

    url(r'^(?P<pk>\d+)/estado$', views.cambiar_estado, name='estado'),
    re_path(r"^(?P<pk>\d+)/tablero/$", views.ver_tablero, name="ver_tablero"),

    re_path(r"^(?P<pk>\d+)/tablero/cambiar_fase/(?P<uid>\d+)$", CambiarFase.as_view(), name="cambiar_fase"),
    re_path(r"^(?P<pk>\d+)/tablero/agregar_comentario/(?P<uid>\d+)$", views.agregar_comentario, name="agregar_comentario"),
    re_path(r"^(?P<pk>\d+)/tablero/horas/(?P<uid>\d+)$", horasTrabajadas.as_view(), name="horas"),

    url(r'^(?P<pk>\d+)/team/', include("team.urls", namespace="team")),
    url(r'^(?P<pk>\d+)/userstory/', include("userstory.urls", namespace="userstory")),
     url(r'^(?P<pk>\d+)/tipous/', include("tipous.urls", namespace="tipous")),
    # Acceso al Sprint
    url(r'^(?P<pk>\d+)/sprint/', include("sprint.urls", namespace="sprint")),

    url(r'^(?P<pk>\d+)/reporte', GeneratePDFhorasTrabajadas.as_view(),name='reporte'),
    url(r'^(?P<pk>\d+)/desempenho', GeneratePDFDesempenho.as_view(),name='reporte_desempenho'),
    url(r'^(?P<pk>\d+)/historial/', HistorialProyecto.as_view(), name='historial'),
]
