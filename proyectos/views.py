import datetime

from django.contrib.admin.options import get_content_type_for_model
from django.contrib.auth.models import User,Group
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.utils.encoding import force_text
from django.views import generic, View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from proyectos.models import Proyecto
from sgp.utils import render_to_pdf
from sprint.models import Sprint
from proyectos.models import Proyecto, Auditoria
from sgp.views import historial
from team.models import team_member
from tipous.models import TipoUS, Fase
from userstory.models import UserStory, Comentario

"""
Todas las vistas de la app proyectos 
Actualmente soporta las siguientes 4 vistas:


1. **ProyectoDetailView** - Detalle de un proyecto
2. **ProyectoCreateView** - Alta de un nuevo proyecto
3. **ProyectoUpdateView** - Modificación de atributos de un proyecto existente
4. **ProyectoDeleteView** - Baja de un proyecto existente
5. **EstadoUpdateView** - Modifica el estado de un proyecto
6. **verTablero** - Visualización de los tableros del proyecto
7. **CambiarFase** - Cambia la fase de un userstory en el tablero
7. **editarComentario** - Edición de un comentario
8. **eliminarComentario - Baja de un comentario
9. **horasTrabajadas** - Permite añadir las horas trabajadas en un US

"""


#Las pruebas unitarias de las vistas se encuentran en [[proyectos/tests.py]]

#=== adicion ===
def adicion(request,*args,**kwargs):
    """
    **Parámetros:**

    1. request: La solicitud del host cliente a la URL.

    2. *args: Argumentos que utilizará la función y que los pasará al CreateView.

    3. **kwargs: Argumentos ID que también se pasan al CreateView.

    **Retorna:** Clase como view ProyectoUpdateView.as_view()

        En caso de comprobar que el usuario "logueado" posea los permisos necesarios, despliega un formulario para

    la modificación de un proyecto existente. En caso de que el usuario no cuente con los permisos se genera una página de error.-
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.add_proyecto')
    if has_permission:
        return ProyectoCreateView.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


#=== modificacion ===
def modificacion(request,*args,**kwargs):
    """
    **Parámetros:**

    1. request: La solicitud del host cliente a la URL.

    2. *args: Argumentos que utilizará la función y que los pasará a la View.

    3. **kwargs: Argumentos ID que también se pasan a la View.

    **Retorna:** Clase como view ProyectoUpdateView.as_view()

        En caso de comprobar que el usuario "logueado" posea los permisos necesarios, despliega un formulario para

    la modificación de un proyecto existente. En caso de que el usuario no cuente con los permisos se genera una página de error.-
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.change_proyecto')
    has_permission|= request.user.has_perm('proyectos.change_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return ProyectoUpdateView.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== cambiar_estado
def cambiar_estado(request,*args,**kwargs):
    """
    **Parámetros:**

    1. request: La solicitud del host cliente a la URL.

    2. *args: Argumentos que utilizará la función y que los pasará al View.

    3. **kwargs: Argumentos ID que también se pasan al View.

    **Retorna:** Clase como view EstadoUpdateView.as_view()

        En caso de comprobar que el usuario "logueado" posea los permisos necesarios, despliega un formulario para

    la modificación de un proyecto existente. En caso de que el usuario no cuente con los permisos se genera una página de error.-
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.change_proyecto')
    has_permission|= request.user.has_perm('proyectos.change_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return EstadoUpdateView.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== baja ===
def baja(request,*args,**kwargs):
    """
    **Parámetros:**

    1. request: La solicitud del host cliente a la URL.

    2. *args: Argumentos que utilizará la función y que los pasará al CreateView.

    3. **kwargs: Argumentos ID que también se pasan al CreateView.

    **Retorna:** Clase como view ProyectoDeleteView.as_view()

        En caso de comprobar que el usuario "logueado" posea los permisos necesarios, despliega una pantalla para

    la eliminación de un proyecto existente. En caso de que el usuario no cuente con los permisos se genera una página de error.-
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.delete_proyecto')
    has_permission|= request.user.has_perm('proyectos.delete_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return ProyectoDeleteView.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


#=== detalle ===
def detalle(request,*args,**kwargs):
    """
    **Parámetros:**

    1. request: La solicitud del host cliente a la URL.

    2. *args: Argumentos que utilizará la función y que los pasará al CreateView.

    3. **kwargs: Argumentos ID que también se pasan al CreateView.

    **Retorna:** Clase como view ProyectoDetailView.as_view()

        En caso de comprobar que el usuario "logueado" posea los permisos necesarios, se visualiza los detalles de un

    proyecto con todos sus componentes y datos esenciales, así como accesos a éstos según el rol que corresponda.

    En caso de que el usuario no cuente con los permisos se genera una página de error.-
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.view_proyecto')
    has_permission|= request.user.has_perm('proyectos.view_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return ProyectoDetailView.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


class ProyectoCreateView(CreateView):
    """
    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. CreateView: Vista para la creación de una nueva instancia de un objeto con una respuesta renderizada por un template

    **Retorna:** URL a ProyectoDetailView
    """
    model = Proyecto
    fields = ["nombre", "descripcion", "logo"]

    def form_valid(self, form):
        response = super(ProyectoCreateView, self).form_valid(form)
        creador_del_poyecto=self.request.user
        rol_SM=Group.objects.get(name="Scrum Master")
        team_member.objects.create(usuario=creador_del_poyecto, rol=rol_SM, proyecto=self.object)
        historial(self, ADDITION, 'Nuevo Proyecto')
        return response


class ProyectoUpdateView(UpdateView):
    model = Proyecto
    template_name_suffix = "_update_form"
    fields = ["descripcion", "logo"]
    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.object.id
        log(self,CHANGE,'Modificacion de proyecto')
        return reverse_lazy("proyectos:detalle",kwargs = {'pk': l})

class EstadoUpdateView(UpdateView):
    model = Proyecto
    fields = ["estado"]
    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.object.id
        if self.object.estado == 'Proyecto Cancelado' or self.object.estado == 'Finalizado':
            query = Sprint.objects.filter(proyecto=self.object.id, estado='En ejecución')
            if query.count() > 0:
                sprint = query.get()
                sprint.estado = 'Finalizado'
                sprint.fechaFinalizacion = datetime.date.today()
                sprint.save()
            query=UserStory.objects.filter(Q(proyecto=self.object.id),Q(estado=0) | Q(estado=1) | Q(estado=2))
            if query.count() > 0:
                for us in query:
                    us.estado=4
                    us.save()

        return reverse_lazy("proyectos:detalle",kwargs = {'pk': l})

    def form_valid(self, form):
        cambios = {
            "En espera de inicio": ["Activo", "Proyecto Cancelado"],
            "Activo": ["Finalizado", "Proyecto Cancelado"],
            "Finalizado": [],
            "Proyecto Cancelado": [],
        }
        old = Proyecto.objects.get(id=self.object.id).estado
        new = form.instance.estado
        if not (old in cambios and new in cambios[old]):
            form.instance.estado = old
        elif new=="Activo":
            form.instance.fecha_inicio = datetime.date.today()
        elif new=="Finalizado":
            form.instance.fecha_fin = datetime.date.today()
        return super().form_valid(form)

    def form_invalid(self, form):
        return self.get_success_url()



class ProyectoDeleteView(DeleteView):
    model = Proyecto
    def get_success_url(self):
        log(self,DELETION,'Borrado de proyecto')
        historial(self,DELETION,'Borrado de proyecto')
        return reverse_lazy("principal")


class ProyectoDetailView(generic.DetailView):
    model = Proyecto
    template_name = "proyectos/detalle.html"

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(generic.DetailView,self).get_context_data(**kwargs)
        context['permiso_view_sprint'] = self.request.user.has_perm('proyectos.view_Sprint')
        context['permiso_view_sprint']|= self.request.user.has_perm('proyectos.view_Sprint',
                                                                    self.object)
        return context


def ver_tablero(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega los tableros con los tipos de User Story del sprint, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_tablero')
    has_permission |= request.user.has_perm('proyectos.ver_tablero', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return verTablero.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === verTablero ====
class verTablero(DetailView):
    """
        Visualizador tableros del proyecto.
        """
    # `Extends:` DetailView; para la visualizacion de los tableros.
    model = Proyecto
    template_name = "proyectos/ver_tablero.html"

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el tablero
        """
        context = super(DetailView,self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        sprint_list = Sprint.objects.filter(proyecto=context['proyecto'])
        tus_list = TipoUS.objects.filter(proyecto=context['proyecto'])
        us_list = UserStory.objects.filter(Q(proyecto=context["proyecto"]) & (Q(estado=0) | Q(estado=2)))
        context['sprint_list'] = sprint_list
        context['tus_list'] = tus_list
        context['us_list'] = us_list

        return context
# === CambiarFase ====
class CambiarFase(UpdateView):
    model = UserStory
    fields = ['fase_actual','subfase']


    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        usid = self.kwargs["uid"]
        us = UserStory.objects.get(id=usid)
        mayor = -1
        for fase in us.tipo_us.fase_set.all():
            if fase.orden > mayor:
                mayor = fase.orden

        if(us.fase_actual.orden == mayor and us.subfase == 2):
            us.estado = 2
            us.save()
            sendEmail(self.kwargs)

        log(self,CHANGE,"Cambio de estado de un US")


        pr=self.kwargs['pk']
        return reverse_lazy("proyectos:ver_tablero",kwargs = {'pk': pr})

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del User Story a modificar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['uid'])
        obj = queryset.get()
        return obj

    def post(self, request, *args, **kwargs):
        """
        `Override:` Define las acciones para cambiar de estado, posterior
        a las validaciones necesarias.
        """
        self.object = self.get_object()
        us = self.object
        vector = request.POST

        sgte_paso = us.team_member.usuario.id == request.user.id

        if sgte_paso:
            sgte_paso = ("fase_actual" in vector)
            sgte_paso = sgte_paso and ("subfase" in vector) and ("accion" in vector)


        if sgte_paso:
            fase= int(vector["fase_actual"])
            subfase = int(vector["subfase"])
            accion = vector["accion"]
            sgte_paso = accion=="izquierda" or accion == "derecha"
            sgte_paso = fase == us.fase_actual_id and sgte_paso
            sgte_paso = subfase == us.subfase and sgte_paso

        if sgte_paso:
            orden_actual = Fase.objects.get(id=us.fase_actual_id).orden
            if accion == "izquierda":
                query = Fase.objects.filter(tipous=us.tipo_us, orden=orden_actual - 1)
                if subfase == 0 and query.count() == 1:
                    us.fase_actual = query.get()
                    us.subfase = 2
                elif subfase != 0:
                    us.subfase = us.subfase - 1
            else:
                query = Fase.objects.filter(tipous=us.tipo_us, orden=orden_actual + 1)
                if subfase == 2 and query.count()==1:
                    us.fase_actual = query.get()
                    us.subfase = 0
                elif subfase != 2:
                    us.subfase = us.subfase + 1

            cambio = "Cambio de Fase"
            autor = us.team_member
            us.historialus_set.create(nombre=us.nombre, descripcion_corta=us.descripcion_corta,
                                      descripcion_larga=us.descripcion_larga, team_member=autor, prioridad=us.prioridad,
                                      prioridad_final=us.prioridad_final, tiempo_estimado=us.tiempo_estimado,
                                      tipo_us=us.tipo_us, valor_negocio=us.valor_negocio,
                                      valor_tecnico=us.valor_tecnico, fecha_modificacion=us.fecha_modificacion,
                                      tiempo_dedicado=us.tiempo_dedicado, fase_actual=us.fase_actual,
                                      subfase=us.subfase, estado=us.estado, cambio_realizado=cambio)
            us.save()
        return HttpResponseRedirect(self.get_success_url())



def sendEmail(kwargs):

    try:
        us = UserStory.objects.get(id=kwargs["uid"])
        proyecto = Proyecto.objects.get(id=kwargs["pk"])
        rol = Group.objects.get(name="Scrum Master")
        member = team_member.objects.get(rol=rol, proyecto=proyecto)
        query = User.objects.filter(id=member.usuario_id)
        if query.count() > 0 :
            scrum_master = query.get()
            email = scrum_master.email
            html_content = "El usuario %s envió el User Story %s a control de calidad para su aprobación en el proyecto %s.<br>"
            message = EmailMessage(subject="US en Control de Calidad", body=html_content % (us.team_member.usuario.username, us.nombre, proyecto.nombre), to=[email])
            message.content_subtype = 'html'
            message.send()
    except :
        print("No se envió el correo de notificación ")

# === HistorialProyecto ====
class HistorialProyecto(ListView):
    model=Auditoria
    template_name = 'proyectos/historial.html'
    paginate_by = 20

    def get_queryset(self):
        """
        `Override:` personaliza la lista de logs por proyecto.
        """
        return Auditoria.objects.filter(proyecto=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context
# === log ====
def log (self,flag,mensaje):
    """
        Crea registros para el historial por proyecto.
    """
    Auditoria.objects.create(
        user_id=self.request.user.pk,
        content_type_id=get_content_type_for_model(self.object).pk,
        object_id=self.object.id,
        object_repr=force_text(self.object),
        action_flag=flag,
        change_message=mensaje,
        proyecto=Proyecto.objects.get(id=self.kwargs['pk']),
    )


def agregar_comentario(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    se añade un nuevo comentario al user story.
    """
    #`Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return agregarComentario.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === agregarComentario ===
class agregarComentario(CreateView):
    """
    Registro de comentarios de un User Story.
    """
    # `Extends:` CreateView; para las operaciones de alta.
    model = Comentario
    fields = ['comentario']

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega al nuevo comentario el indicador implicito del us al
        que pertenece y del usuario encargado del us.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.us = UserStory.objects.get(id=kwargs['uid'])
        form.instance.autor = User.objects.get(id=form.instance.us.team_member.usuario.id)
        print("autor: ", form.instance.autor.username)
        us = UserStory.objects.get(id=kwargs['uid'])
        cambio = "Comentario agregado"
        autor = us.team_member
        us.historialus_set.create(nombre=us.nombre, descripcion_corta=us.descripcion_corta, team_member=autor,
                                  descripcion_larga=us.descripcion_larga, prioridad=us.prioridad,
                                  prioridad_final=us.prioridad_final, tiempo_estimado=us.tiempo_estimado,
                                  tipo_us=us.tipo_us, valor_negocio=us.valor_negocio, valor_tecnico=us.valor_tecnico,
                                  fecha_modificacion=us.fecha_modificacion, tiempo_dedicado=us.tiempo_dedicado,
                                  fase_actual=us.fase_actual, subfase=us.subfase, estado=us.estado, cambio_realizado=cambio)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        log(self, ADDITION, 'Nuevo registro de comentario')
        return reverse_lazy("proyectos:ver_tablero",kwargs = {'pk': pr})


def editar_comentario(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la edicion de los comentarios,
    o un mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return editarComentario.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === editarComentario ===
class editarComentario(UpdateView):
    """
    Recepcion de la edicion de un comentario.
    """
    # `Extends:` UpdateView; para las operaciones de modificacion.
    model = Comentario
    fields = ['comentario']

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del comentario a editar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['c'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        us = self.kwargs['tmid']
        log(self, CHANGE, 'Edicion de comentario en US')
        return reverse_lazy("proyectos:userstory:detalle", kwargs={'pk': pr, 'tmid': us})


def eliminar_comentario(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un pedido de confirmacion para proceder a borrar el item
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return eliminarComentario.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === eliminarComentario ===
class eliminarComentario(DeleteView):
    """
    Recepciona la solicitud de borrado de un comentario.
    """
    # `Extends:` DeleteView; para la operación de baja.
    model = Comentario

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del comentario a eliminar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['c'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        us = self.kwargs['tmid']
        log(self,DELETION,'Borrado de un comentario en US')
        return reverse_lazy("proyectos:userstory:detalle", kwargs={'pk': pr, 'tmid': us})


# === horasTrabajadas ===
class horasTrabajadas(UpdateView):
    """
    Recepcion de las horas trabajadas en un user story.
    """
    # `Extends:` UpdateView; para las operaciones de modificacion.
    model = UserStory
    fields = ['tiempo_dedicado']

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del us para modificar las horas dedicadas.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['uid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        log(self, CHANGE, 'Actualizacion de horas trabajadas en US')
        print("Horas cargadas correctamente")
        return reverse_lazy("proyectos:ver_tablero",kwargs = {'pk': pr})

    def form_valid(self, form):
        us = self.get_object()
        t_actual = us.tiempo_dedicado
        t_estimado = us.tiempo_estimado
        t_nuevo = form.cleaned_data['tiempo_dedicado']
        tiempo = int(int(t_actual)+int(t_nuevo))
        print("t_estimado: ",t_estimado, "t_actual:",t_actual ," t_nuevo:", t_nuevo, "tiempo:", tiempo)
        form.instance.tiempo_dedicado = tiempo
        cambio = "Actualización de hs trabajadas"
        autor = us.team_member
        us.historialus_set.create(nombre=us.nombre, descripcion_corta=us.descripcion_corta,
                                  descripcion_larga=us.descripcion_larga, team_member=autor, prioridad=us.prioridad,
                                  prioridad_final=us.prioridad_final, tiempo_estimado=us.tiempo_estimado,
                                  tipo_us=us.tipo_us, valor_negocio=us.valor_negocio, valor_tecnico=us.valor_tecnico,
                                  fecha_modificacion=us.fecha_modificacion, tiempo_dedicado=tiempo,
                                  fase_actual=us.fase_actual, subfase=us.subfase, estado=us.estado,
                                  cambio_realizado=cambio)
        us.save()
        self.object = form.save()
        return super(horasTrabajadas, self).form_valid(form)


#=== GeneratePDFSprintBacklog ==
class GeneratePDFhorasTrabajadas(View):
    """
         Genera un pdf con el Sprint backlog de un Sprint.
    """
    model=team_member
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteHorasTrabajadas.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteHorasTrabajadas.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        lista_team=team_member.objects.filter(proyecto=self.kwargs['pk'])
        print(lista_team)
        context = {
            "hora": datetime.datetime.now(),
            "proyecto":Proyecto.objects.get(id=self.kwargs['pk']),
            "team":lista_team,
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteHorasTrabajadas.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("Horas trabajadas")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(View,self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        return context

#=== GeneratePDFDesempenho ==
class GeneratePDFDesempenho(View):
    """
         Genera un pdf con el Desempeño del equipo.
    """
    model=team_member
    permission_required = 'proyectos.ver_Sprint'
    raise_exception = True
    template_name = 'pdf/reporteDesempenho.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """

        template = get_template('pdf/reporteDesempenho.html')

        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        cabezal = ['Actividad']
        ubi = {}
        cant=0
        for tm in self.proyecto.team_member_set.all():
            cant= cant + 1
            cabezal += [tm.usuario.username]
            ubi[tm]= cant
        cabezal += ['Total']
        values = []
        for sprint in self.proyecto.sprint_set.exclude(estado="En espera"):
            sprint_values = []
            inicio = sprint.fechaInicio
            if sprint.estado == "En ejecución":
                fin = None
            else:
                fin = sprint.fechaFinalizacion
            for us in sprint.sprintBacklog.all():
                aux = [us.nombre]
                for i in range(0,cant):
                    aux+=[0]
                v_ant=None
                for v_new in us.historialus_set.all():
                    comparar_registros(v_new,v_ant,inicio,fin,aux,ubi)
                    v_ant= v_new
                comparar_registros(us,v_ant,inicio,fin,aux,ubi)
                row_total = 0
                for i in range(0,cant):
                    row_total += aux[i+1]
                sprint_values+= [aux+[row_total]]
            subtotal=[]
            for j in range(0,cant+1):
                columna_total = 0
                for i in sprint_values:
                    columna_total += i[j+1]
                subtotal+=[columna_total]
            sprint_values += [['Subtotal(Sprint)']+subtotal]
            values += sprint_values

        total = []
        for j in range(0, cant + 1):
            columna_total = 0
            for i in values:
                columna_total += i[j + 1]
            total += [int(columna_total/2)]
        values += [['Total(Proyecto)'] + total]

        context = {
            "hora": datetime.datetime.now(),
            "proyecto": self.proyecto,
            "cabeza": cabezal,
            "matriz": values,
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteDesempenho.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("Desempenho")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(View,self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        return context

# === has_perm ===
def has_perm(user,perm,proy_id):
    #Funcion auxiliar para verificar que el usuario `user` tenga el permiso `perm`
    #en el proyecto de id `proy_id`.
    """
    `Return:` True si posee permisos, False si no.
    """
    has = user.has_perm(perm, Proyecto.objects.get(id=proy_id))
    print(perm+"? "+str(has))
    return has

def comparar_registros(version_actual,version_anterior,
                       fecha_inicio,fecha_fin,diccionario,ubi):
    try:
        ultima=version_actual.fecha_modificacion.date()
        if version_actual.team_member in ubi:
            key = ubi[version_actual.team_member]
            if ultima>=fecha_inicio and (fecha_fin == None or ultima<=fecha_fin):
                diccionario[key] = diccionario[key] + version_actual.tiempo_dedicado
                if version_anterior != None:
                    diccionario[key] = diccionario[key] - version_anterior.tiempo_dedicado
    except:
        print("Fin de Linea")