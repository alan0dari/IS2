from django.contrib.auth.models import User, Group
from django.test import TestCase, RequestFactory
from django.test import Client
from poblar import poblar
from proyectos.models import Proyecto
from team.models import team_member
from tipous.models import TipoUS, Fase
from userstory.models import UserStory

"""
Todas las pruebas de la aplicacion proyectos
Actualmente soporta las siguientes 5 pruebas:


1. **test_AltaProyecto** 
2. **test_ModificacionProyecto** 
3. **test_BajaProyecto**
4. **test_DetalleProyecto**

**Observaciones:**

1. Correr cada clase Test de forma independiente,test_ModificacionProyecto y test_BajaProyecto correr una función a la vez
"""

c = Client()


class test_AltaProyecto(TestCase):
    # === test_AltaProyecto ===
    def setUp(self):
        self.user = User.objects.create_superuser(username='Pepe', email='pepe@gmail.com', password='adminadminadmin')
        Group.objects.get_or_create(name='Scrum Master')

        User.objects.create(id=100, username="usuario_activo")

    def test_Registrar_valido(self):
        self.client.login(username='Pepe', password='adminadminadmin')
        response = self.client.post('/proyecto/registrar/', data={'nombre': 'Cerro', 'descripcion': 'La mitad + 1'})

        proyecto = Proyecto.objects.get(nombre="Cerro")
        print(Proyecto.objects.all())
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)
        print('Test de Creacion de Proyecto ejecutado exitosamente.\n')

        self.assertEqual(response.status_code, 302)

    def test_Registro_invalido(self):
        self.client.login(username='Pepe', password='adminadminadmin')
        # No tiene campo nombre
        self.client.post('/proyecto/registrar/', data={'nombre': 'Olimpia', 'descripcion': 'El expreso decano'})
        response = self.client.post('/proyectos/registrar/', data={'descripcion': 'La mitad + 1'})

        proyecto = Proyecto.objects.last()
        print(proyecto.nombre)
        print(response.status_code)
        print('Si el registro fue exitoso retorna 302 y si el registro fue inválido retorna 404 como codigo de estado')
        self.assertEqual(response.status_code, 404)
        print('Si el registro falla como se espera, el ultimo proyecto fue Olimpia')
        self.assertEqual(proyecto.nombre, 'Olimpia')
        print('Test de Registro inválido de Proyecto ejecutado exitosamente. No se registró proyecto nuevo.\n')


class test_ModificacionProyecto(TestCase):
    # === test_ModificacionProyecto ===
    def setUp(self):
        self.user = User.objects.create_superuser(username='Jose', email='jose@gmail.com', password='adminadminadmin')
        Group.objects.get_or_create(name='Scrum Master')

        User.objects.create(id=100, username="usuario_activo")

    def test_ModificacionProyecto_valido(self):
        print('Inicio de Test de Modificación de Proyecto Válida')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Guaraní', 'descripcion': 'El más viejazo'})

        proyecto = Proyecto.objects.get(nombre='Guaraní')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        response = self.client.post('/proyecto/' + str(proyecto.id) + '/editar/', data={'nombre': 'Guaraní',
                                                                                        'descripcion': 'Descripción editada'})

        print('Después de la edición')
        proyecto = Proyecto.objects.get(nombre='Guaraní')
        print('Proyecto editado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)
        print('Test de Modificación de Proyectos Válida ejecutado exitosamente.\n')

        self.assertEqual(response.status_code, 302)

        response =self.client.post('/proyecto/' + str(proyecto.id) + '/estado',
                                   data={'estado': "Activo"})
        print(response)
        self.assertEquals(Proyecto.objects.get(id=proyecto.id).estado,
                          "Activo",
                          "No se cambio el estado")


    def test_ModificacionProyecto_invalido(self):
        print('Inicio de Test de Modificación de Proyecto Inválida')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Guaraní', 'descripcion': 'El más viejazo'})

        proyecto = Proyecto.objects.get(nombre='Guaraní')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        # No se proporciona campos críticos
        response = self.client.post('/proyecto/' + str(proyecto.id) + '/editar/', data={})

        print('Después de la edición')
        proyecto = Proyecto.objects.get(nombre='Guaraní')
        print('Proyecto editado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)
        print('Test de Modificación de Proyectos Inválida ejecutado exitosamente.\n')

        self.assertEqual(response.status_code, 200)

        try:
            self.client.post('/proyecto/' + str(proyecto.id) + '/estado',
                                   data={'estado': "Finalizado"})
        except:
            print("Fallo en el post")
        self.assertNotEquals(Proyecto.objects.get(id=proyecto.id).estado,
                          "Finalizado",
                          "Se cambio el estado")


# === test_BajaProyecto ===
class test_BajaProyecto(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(username='Jose', email='jose@gmail.com', password='adminadminadmin')
        Group.objects.get_or_create(name='Scrum Master')

        User.objects.create(id=100, username="usuario_activo")

    def test_BajaProyecto_valido(self):
        print('Inicio de Test de Baja de Proyecto válida')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Luque', 'descripcion': 'Tu cel kape'})

        proyecto = Proyecto.objects.get(nombre='Luque')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        self.client.login(username='Jose', password='adminadminadmin')

        print('Antes del borrado')
        print(Proyecto.objects.all())
        response = self.client.post('/proyecto/' + str(proyecto.id) + '/eliminar/')
        print('Despues del borrado')
        print(Proyecto.objects.all())
        print('Test de Baja de Proyecto ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 302)

    def test_BajaProyecto_invalido(self):
        print('Inicio de Test de Baja de Proyecto inválida')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Luque', 'descripcion': 'Tu cel kape'})

        proyecto = Proyecto.objects.get(nombre='Luque')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        self.client.login(username='Jose', password='adminadminadmin')

        print('Antes del borrado')
        print(Proyecto.objects.all())
        # Se intenta borrar un proyecto que no existe
        print('Se suministra un pk inexistente en la url.\n')
        print('No se puede eliminar el proyecto con id 10, tal proyecto no existe.')
        print('Test de Baja de Proyecto ejecutado exitosamente.\n')


# === test_DetalleProyecto ===

class test_DetalleProyecto(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Jose', email='jose@gmail.com', password='adminadminadmin')
        self.user = User.objects.create_user(username='Pepe', email='pepe@gmail.com', password='adminadminadmin')
        Group.objects.get_or_create(name='Scrum Master')

        User.objects.create(id=100, username="usuario_activo")

    def DetalleProyecto_valido(self):
        print('Inicio de Test de Detalle de Proyecto válida')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Nacional', 'descripcion': 'Sin gente'})

        proyecto = Proyecto.objects.get(nombre='Nacional')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        print('Se accede a los detalles del proyecto')
        response = self.client.post('/proyecto/' + str(proyecto.id) + '/')

        print('Test de Detalle de Proyecto ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 200)

    def DetalleProyecto_invalido(self):
        print('Inicio de Test de Detalle de Proyecto inválida')
        print('José se loguea')
        self.client.login(username='Jose', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'Nacional', 'descripcion': 'Sin gente'})

        proyecto = Proyecto.objects.get(nombre='Nacional')
        print('Proyecto creado: ')
        print('\tNombre: ' + proyecto.nombre)
        print('\tDescripción: ' + proyecto.descripcion)
        print('\tFecha de creación: ' + str(proyecto.fecha_creacion))
        print('\tFecha de inicio: ' + str(proyecto.fecha_inicio))
        print('\tFecha de finalización: ' + str(proyecto.fecha_fin))
        print('\tÚltima modificación: ' + str(proyecto.ultima_modificacion))
        print('\tEstado: ' + proyecto.estado)

        print('Pepe se loguea')
        self.client.login(username='Pepe', password='adminadminadmin')

        print('Intenta acceder a los detalles del proyecto')
        response = self.client.post('/proyecto/' + str(proyecto.id) + '/')
        print('Acceso denegado')

        print('Test de Detalle de Proyecto ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 200)


# === test_verTablero ===
class test_verTablero(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(username='Nati', email='nati@gmail.com', password='adminadminadmin')
        Group.objects.get_or_create(name='Scrum Master')
        User.objects.create(id=100, username="usuario_activo")

    def test_ver_tablero_valido(self):
        print('Inicio de test ver tablero válido')
        self.client.login(username='Nati', password='adminadminadmin')
        self.client.post('/proyecto/registrar/', data={'nombre': 'SGP', 'descripcion': 'asdasd'})

        proyecto = Proyecto.objects.get(nombre="SGP")

        print('Se accede a los tableros del proyecto')
        respuesta = self.client.get('/proyecto/' + str(proyecto.id) + '/tablero/')
        self.assertTrue(respuesta.status_code == 200, msg="No se muestran los tableros del proyecto")
        print('Los tableros se muestran correctamente.\n')

    def test_ver_tablero_invalido(self):
        print('Inicio de test ver tablero inválido')
        url = "proyecto/90/tablero/"
        print("Se intenta acceder a tableros de un proyecto que no existe")
        respuesta = self.client.get(url)
        self.assertTrue(respuesta.status_code == 404, msg="Se muestran los tableros del proyecto")
        print("Como se espera no se muestran los tableros del proyecto porque no existe")
# === test_cambiarfase ===
class test_cambiarfase(TestCase):
    def setUp(self):
        poblar()

    def test_cambiarfase(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_us=UserStory.objects.get(nombre="US 3",proyecto=id_proyecto).id
        us=UserStory.objects.get(nombre="US 3",proyecto=id_proyecto)
        us.tipo_us = TipoUS.objects.get(nombre="Base", proyecto=id_proyecto)
        us.team_member=team_member.objects.get(proyecto=Proyecto.objects.get(nombre="SGP"),usuario=User.objects.get(username='Eduardo'))
        us.fase_actual=Fase.objects.get(tipous=TipoUS.objects.get(nombre="Base", proyecto=id_proyecto),nombre="Inicio")
        print(Fase.objects.get(tipous=TipoUS.objects.get(nombre="Base", proyecto=id_proyecto),nombre="Inicio").id)
        us.save()

        url = "/proyecto/" + str(id_proyecto) + "/tablero/cambiar_fase/" +str(id_us)
        datos={
            "fase_actual":Fase.objects.get(tipous=TipoUS.objects.get(nombre="Base", proyecto=id_proyecto),nombre="Inicio").id,
            "subfase":0,
            "accion":'derecha',
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=datos)
        query=UserStory.objects.filter(pk=id_us)
        if query.count() >0:
            usCambiado=query.get()
            self.assertEqual(usCambiado.subfase,1)
        self.assertEquals(respuesta.status_code, 302, msg="Error en el cambio de fase")