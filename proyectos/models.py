from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Proyecto(models.Model):
    class Meta:
        """
            Meta-clase de Proyecto:
        Aquí se situan los parámetros de orden del modelo de proyecto y permisos de
        todos las clases que componen un proyecto.
        """
        permissions = (
            ('view_proyecto', 'Ver detalles de un proyecto'),
            ('ver_tablero', 'Ver tablero del proyecto'),
            ('view_historial', 'Ver historial de un proyecto'),
            ('add_Integrante', 'Agregar un integrante al Team'),
            ('edit_Integrante', 'Editar datos de integrante'),
            ('delete_Integrante', 'Eliminar un integrante'),
            ('add_Tus', 'Registrar un tipo de user story'),
            ('edit_Tus', 'Editar datos de un tipo de userstory'),
            ('delete_Tus', 'Eliminar un tipo de user story'),
            ('fases_Tus', 'Manipulacion de las fases del tipo de user story'),
            ('view_Sprint', 'Ver Sprints'),
            ('ver_control','Ver control de calidad en un sprint'),
            ('add_Sprint', 'Crear un Sprint'),
            ('delete_Sprint', 'Descartar un Sprint'),
            ('edit_Sprint', 'Editar Sprint y administrar registro de Reuniones'),
            ('ver_UserStory', 'Ver detalles de un user story'),
            ('ver_ProductBacklog', 'Ver product backlog'),
            ('ver_USfinalizados', 'Ver user stories finalizados'),
            ('crear_UserStory', 'Crear user story'),
            ('modificar_UserStory', 'Modificar user story'),
            ('eliminar_UserStory', 'Eliminar user story'),
        )

        ordering = ["estado","id"]

    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=1000)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_inicio = models.DateTimeField(null="0000-00-00")
    fecha_fin = models.DateTimeField(null="0000-00-00")
    ultima_modificacion = models.DateTimeField(auto_now=True)
    estado = models.CharField(max_length=20, default="En espera de inicio")
    logo = models.FileField(default=None, blank=True)

    def __str__(self):
        return "Nombre: " + self.nombre + "\t\tInicio: " + str(self.fecha_inicio) + "\t\tFin: " + str(
            self.fecha_fin) + "\t\tEstado: " + str(self.estado) + "\n"

    def get_absolute_url(self):
        return reverse("proyectos:detalle", kwargs={"pk": self.pk})



class Auditoria(LogEntry):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.SET_NULL,null=True)