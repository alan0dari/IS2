from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django import forms
from django.db.models import Q
"""
Todos los form de la aplicacion usuarios 
Actualmente soporta las siguientes 2 form:


1. **FormUsuario** -usado para la edicion
2. **UserCreationForm** - usado para el registro

"""

#Los form son utilizados en las view [[views.py]]

class FormUsuario(forms.ModelForm):


    class Meta:
        model= User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'is_active'
        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name': 'Nombre',
            'last_name': 'Apellidos',
            'email': 'Correo',
            'is_active':'Activo',
        }

        widgets= {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'})
        }

class UserCreationForm(forms.ModelForm):

    groups = forms.ModelChoiceField(queryset=Group.objects.filter(Q(name='Administrador') | Q(name='Usuario Normal')),
                                    required=True,widget=forms.Select(attrs={'class':'form-control'}))
    class Meta:
            model = User
            fields = [
                'username',
                'first_name',
                'last_name',
                'email',
                'groups',
            ]
            labels = {
                'username': 'Nombre de usuario',
                'first_name': 'Nombre',
                'last_name': 'Apellidos',
                'email': 'Correo',
                'groups': 'Grupos',
            }
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        default_password = 'Cambiar.123'
        user.set_password(default_password)
        if commit:
            user.save()
        return user















