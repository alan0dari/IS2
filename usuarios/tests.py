from django.contrib.auth.models import User,Group
from django.test import TestCase, RequestFactory
from .views import RegistroUsuario
from django.test import Client
"""
Todas las pruebas de la aplicacion usuarios
Actualmente soporta las siguientes 3 pruebas:


1. **test_Registrar** 
2. **test_modificacion** 
3. **test_Borrado**


"""


class test_usuario(TestCase):
    def setUp(self):
        poblar()
        # === test_Registrar ===
    def test_registro_valido(self):
        url='/usuarios/registrar'
        registro={'username': 'olimpia',
                  'email': 'lalala@gmail.com',
                  'first_name': 'Guillermo',
                  'last_name': 'Caballero',
                  'groups': Group.objects.get(name='Usuario Normal').id
        }
        print("Post to: " + url)
        print("Data " + str(registro))
        self.client.login(username='Guillermo',password='adminadmin')
        respuesta=self.client.post(url,data=registro)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302,msg='No se ha podido registrar un usuario')

    # === test_Registrar invalido ===
    def test_registro_invalido(self):
        url = '/usuarios/registrar'
        antes=User.objects.count()
        registro = {'username': 'ole',
                    'email': 'lalala@gmail.com',
                    'first_name': 'Guillermo',
                    'last_name': 'Caballero',
                    'groups': Group.objects.get(name='Usuario Normal').id
                    }
        print("Post to: " + url)
        print("Data " + str(registro))
        self.client.login(username='Eduardo', password='adminadmin')
        respuesta = self.client.post(url, data=registro)
        print(respuesta.status_code)
        despues=User.objects.count()
        self.assertTrue(antes == despues, msg='Tiene los permisos y se registro correctamente')
        # === test_modificar valido ===
    def test_modificacion_valido(self):
        id_usuario=User.objects.get(username='Alan').id
        url='/usuarios/editar/'+str(id_usuario)+'/'
        registro = {'username': 'Alan',
                    'email': '',
                    'first_name': 'Pato',
                    'last_name': '',
                    'groups': Group.objects.get(name='Usuario Normal').id
                    }
        print("Post to: " + url)
        print("Data " + str(registro))
        self.client.login(username='Guillermo',password='adminadmin')
        respuesta=self.client.post(url,data=registro)
        self.assertTrue(respuesta.status_code==302,msg='No se ha editado correctamente')

    # === test_modificacion invalido ===
    def test_modificacion_invalido(self):
        id_usuario = User.objects.get(username='Alan').id
        url = '/usuarios/editar/' + str(id_usuario) + '/'
        registro = {
                    'email': '',
                    'first_name': 'Pato',
                    'last_name': '',
                    'groups': Group.objects.get(name='Usuario Normal').id
                    }
        print("Post to: " + url)
        print("Data " + str(registro))
        self.client.login(username='Guillermo', password='adminadmin')
        respuesta = self.client.post(url, data=registro)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 200, msg='Se ha editado correctamente')

    # === test_Borrado  ===
    def  test_Borrar_usuario(self):
        id_usuario = User.objects.get(username='Alan').id
        url = '/usuarios/eliminar/' + str(id_usuario) + '/'
        print("Post to: " + url)
        antes=User.objects.count()
        self.client.login(username='Guillermo', password='adminadmin')
        respuesta = self.client.post(url)
        print(respuesta.status_code)
        despues=User.objects.count()
        self.assertTrue(antes > despues, msg='No se ha eliminado correctamente')




from django.contrib.auth.models import User, Permission, Group

from sprint.models import Sprint, reuniones
from team.models import team_member
from proyectos.models import Proyecto
from userstory.models import UserStory
from tipous.models import TipoUS,Fase

def poblar():
    perm_scrum=['add_Integrante','edit_Integrante','delete_Integrante',
                'view_proyecto','change_proyecto','delete_proyecto','add_Tus','edit_Tus','delete_Tus','fases_Tus',
                'add_Sprint', 'delete_Sprint', 'edit_Sprint', 'view_Sprint',
                'ver_UserStory', 'crear_UserStory', 'modificar_UserStory', 'eliminar_UserStory']

    usuarios=["Alan","Eduardo","Guillermo","Natalia"]
    nombre_usuarios=["Alan Mathías","Eduardo","Guillermo Jesús","Natalia De Jesús"]
    apellido_usuarios=["Ruiz Diaz Nodari","Centurion Funes","Caballero Escobar","Trinidad Gimenez"]
    roles=["Scrum Master","Developer","Tester"]
    proyectos=["SGP","ATM","Download Tester"]
    new_users = {}
    new_roles = {}
    new_proyectos = {}
    new_sys_roles= {}
    new_us_sgp = {}
    for i in ["Administrador","Usuario Normal"]:
        query=Group.objects.filter(name=i)
        if query.count()>0:
            new_sys_roles[i]=query.get()
        else:
            new_sys_roles[i]=Group.objects.create(name=i)
    new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename='view_proyecto'))
    new_sys_roles["Usuario Normal"].permissions.add(Permission.objects.get(codename='add_proyecto'))
    for i in ["add","change","delete"]:
        for j in ["group","user"]:
            new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename=i+'_'+j))

    for i in range(0,4):
        try:
            query= User.objects.filter(username=usuarios[i])
            if query.count()==1 :
                new_users[usuarios[i]]=query.get()
            else:
                new_users[usuarios[i]]=User.objects.create_user(username=usuarios[i],
                                                                first_name=nombre_usuarios[i],
                                                                last_name=apellido_usuarios[i],
                                                                password="adminadmin")
        except:
            if User.objects.filter(username=usuarios[i]).count()==0:
                print("No se puedo crear el usuario :"+usuarios[i])
        if usuarios[i] == "Guillermo":
            new_users[usuarios[i]].groups.add(new_sys_roles["Administrador"])
        else:
            new_users[usuarios[i]].groups.add(new_sys_roles["Usuario Normal"])
    for i in roles:
        try:
            query=Group.objects.filter(name=i)
            if query.count()==1:
                new_roles[i]=query.get()
            else:
                new_roles[i]=Group.objects.create(name=i)
            new_roles[i].permissions.add(Permission.objects.get(codename='view_proyecto'))
        except:
            print("No se pudo crear rol"+i)

    for i in perm_scrum:
        try:
            grupo = Group.objects.get(name="Scrum Master")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

    for i in proyectos:
        query=Proyecto.objects.filter(nombre=i)
        if query.count() == 0 :
            new_proyectos[i]=Proyecto.objects.create(nombre=i,descripcion="")
            team_member.objects.create(usuario=new_users["Eduardo"],
                                       rol=new_roles["Scrum Master"],
                                        proyecto=new_proyectos[i])
        else:
            new_proyectos[i] = query.get()
        query=TipoUS.objects.filter(proyecto=new_proyectos[i],nombre="Base")
        if query.count() == 1:
            tus = query.get()
        else:
            tus = TipoUS.objects.create(proyecto=new_proyectos[i],
                                        nombre="Base",
                                        descripcion="TUS base")
        nombre_fase=["Inicio","Fin"]
        for j in range(0,2):
            query = Fase.objects.filter(tipous=tus,nombre=nombre_fase[j])
            if query.count() == 0:
                Fase.objects.create(nombre=nombre_fase[j],tipous=tus,orden=j)



    for i in range(1, 6):
        name = "US " + str(i)
        try:
            query = UserStory.objects.filter(nombre=name, proyecto=new_proyectos["SGP"])
            if query.count() > 0:
                new_us_sgp[name] = query.get()
            else:
                new_us_sgp[name] = UserStory.objects.create(nombre=name,
                                                            descripcion_corta=name,
                                                            descripcion_larga="User Story "+str(i),
                                                            valor_negocio=i,
                                                            valor_tecnico=i,
                                                            proyecto=new_proyectos["SGP"])
        except Exception as e:
            print("Fallo creado UserStory : " + name)
    try:
        query = Sprint.objects.filter(proyecto=new_proyectos["SGP"])
        if query.count() == 0:
            sprint_SGP = Sprint.objects.create(fechaInicio="2018-04-18",
                                               fechaFinalizacion="2019-04-19",
                                               estado="En espera",
                                               proyecto=new_proyectos["SGP"])
            for i in range(1, 4):
                name = "US " + str(i)
                sprint_SGP.sprintBacklog.add(new_us_sgp[name])
            sprint_SGP.save()
        else:
            sprint_SGP = query.get()
            for i in range(1, 4):
                name = "US " + str(i)
                sprint_SGP.sprintBacklog.add(new_us_sgp[name])
            sprint_SGP.save()
    except Exception as e:
        print("Fallo creando Sprint en SGP")

    for i in range(1, 6):
        title = "Reunion " + str(i)
        fecha = "2018-04-" + str(18 + i)
        descripcion = "En la " + title + "..."
        try:
            query = reuniones.objects.filter(titulo=title, sprint=sprint_SGP)
            if query.count() == 0:
                reuniones.objects.create(titulo=title,
                                         fecha=fecha,
                                         resumen=descripcion,
                                         sprint=sprint_SGP)
        except:
            print("Fallo creado Reunion : " + str(title))

    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="add_group"))
    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="change_group"))