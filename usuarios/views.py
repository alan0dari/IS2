from django.contrib.admin.models import ADDITION, CHANGE,DELETION
from django.contrib.auth.models import User, Group
from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView, TemplateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from django.core.mail import EmailMessage

from sgp.views import historial
from .forms import  FormUsuario,UserCreationForm
"""
Todas las vistas de la aplicacion usuarios 
Actualmente soporta las siguientes 5 vistas:


1. **ListarUsuario** - Vista para la lista de usuarios
2. **RegistroUsuario** - Se agrega un nuevo usuario
3. **UpdateUsuario** -Se modifican los Datos del Usuario
4. **UsuarioDelete** - Se eliminan los Datos del Usuario
5. **UsuarioDetailView** - Se visualizan los Datos del Usuario

"""
#Las pruebas unitarias de las vistas se encuentran en [[tests.py]]

# === ListarUsuario ===
class ListarUsuario(PermissionRequiredMixin,ListView):
    """
    **Ancestros**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. ListView: Renderiza alguna lista de objetos,seteado por  `self.model` or `self.queryset`.`self.queryset` puede ser cualquier iterable de items no solo un queryset

    **Retorna:** url a ListarUsuarios
    """


    model = User
    template_name = 'usuarios/listar_usuarios.html'
    paginate_by = 8
    permission_required = 'auth.change_user'
# === RegistroUsuario ===
class RegistroUsuario(PermissionRequiredMixin,CreateView):
    """
    **Ancestros:**

    1. PermissionRequiredMixin:CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. CreateView: Vista para la creacion de una nueva instancia de un objeto con una respuesta renderizada por un template

    **Retorna:** url a ListarUsuarios
    """
    model = User
    template_name = "usuarios/registrar.html"
    permission_required = 'auth.add_user'
    form_class = UserCreationForm

    def form_valid(self, form):
        """
        **Parametros**

           1. self: referencia al objeto

           2. form: Formulario de validacion del usuario

           **Retorna:** response: Formulario Valido
           """
        response = super(RegistroUsuario, self).form_valid(form)
        username=form.cleaned_data['username']
        nombre = form.cleaned_data['groups']
        email= form.cleaned_data['email']
        default_pass='Cambiar.123'
        g = Group.objects.get(name=nombre)
        g.user_set.add(self.object)
        html_content = "Tu usuario es: %s <br> Tu password es: %s <br>Por favor cambie su contraseña"
        message = EmailMessage(subject='Bienvenido al Sistema Gestor de Proyectos', body=html_content % (username, default_pass), to=[email])
        message.content_subtype = 'html'
        message.send()
        return response

    def get_success_url(self):
        """
        **Parametros**

        1. self: referencia al objeto

        **Retorna:** url del ListarUsuario
        """
        historial(self,ADDITION,'Nuevo Usuario')
        return reverse_lazy('usuarios:listar')

# === UpdateUsuario ===
class UpdateUsuario(PermissionRequiredMixin,UpdateView):
    """

    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. UpdateView: Vista para modificar un objeto,con una respuesta renderizada por un template

    **Retorna**: url a ListarUsuarios
    """

    model = User
    form_class = FormUsuario
    permission_required = 'auth.change_user'
    template_name = 'usuarios/usuario_detalle.html'

    def get_success_url(self):
        """
        **Parametros**

        1. self: referencia al objeto

        **Retorna:** url del ListarUsuario
        """
        historial(self,CHANGE,'Modificacion de  Usuario')
        return reverse_lazy('usuarios:listar')

# === UsuarioDelete ===
class UsuarioDelete(PermissionRequiredMixin,DeleteView):
    """

    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. DeleteView: Vista para borrar un objeto recolectado con  `self.get_object()`, con una respuesta renderizada con una template

    **Retorna:** url a ListarUsuarios
    """
    model = User
    permission_required = 'auth.delete_user'
    template_name = 'usuarios/usuario_borrar.html'
    def get_success_url(self):
        """
        **Parametros**

        1. self: referencia al objeto

        **Retorna:** url del ListarUsuario
        """
        historial(self,DELETION,'Borrado Usuario')
        return reverse_lazy('usuarios:listar')
# === UsuarioDetailView ===
class UsuarioDetailView(DetailView):
    """


    **Ancestros:**

    1.DetailView: por defecto esta es una instancia
        de modelo para `self.queryset` pero
        la vista soporta mostrar cualquier
        objeto si se sobreescribe `self.get_object()

    **Retorna:** Renderiza la vista detalle del objeto
    """
    template_name = "usuarios/usuario_form.html"

    def get_context_data(self, *args, **kwargs):
        context = super(UsuarioDetailView, self).get_context_data(*args, **kwargs)

        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        try:
            instance = User.objects.get(id=pk)
        except User.DoesNotExist:
            raise Http404("Not found..")

        print(request.user.pk)
        print(int(pk))
        if int(request.user.pk) == int(pk):
            return instance
        if request.user.has_perm('auth.add_user'):
            return instance















