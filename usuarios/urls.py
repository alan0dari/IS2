from django.contrib import admin
from django.contrib.auth.decorators import permission_required
from django.urls import path
from django.conf.urls import url

from django.contrib.auth.decorators import login_required

from .views import RegistroUsuario, ListarUsuario, UpdateUsuario, UsuarioDelete,UsuarioDetailView

app_name = 'usuarios'
urlpatterns = [
    url(r'^listar$', ListarUsuario.as_view(), name='listar'),
    url(r'^registrar',RegistroUsuario.as_view(), name="registrar"),
    url(r'^editar/(?P<pk>\d+)/$', UpdateUsuario.as_view(), name='editar'),
    url(r'^eliminar/(?P<pk>\d+)/$', UsuarioDelete.as_view(), name='eliminar'),
    url(r'^ver/(?P<pk>\d+)/$',login_required(UsuarioDetailView.as_view()), name='ver'),
]