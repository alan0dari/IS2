from django.contrib import admin

from userstory.models import UserStory

admin.site.register(UserStory)
