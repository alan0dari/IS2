from django.urls import path,include
from django.conf.urls import url

from proyectos.views import editar_comentario, eliminar_comentario
from .views import listar_us, crear_us, modificar_us, eliminar_us, detalle_us, HistorialUSListView, listar_pb, \
    listar_us_finalizados, GeneratePDF, ReporteCambioFaseUS, ReporteHistorialUS

app_name = 'userstory'

urlpatterns = [
    url(r'^$', listar_us, name='listar'),
    url(r'^crear_us$', crear_us, name='crear'),
    url(r'^modificar_us/(?P<tmid>\d+)/$', modificar_us, name='modificar'),
    url(r'^eliminar_us/(?P<tmid>\d+)/$', eliminar_us, name='eliminar'),
    url(r'^detalle_us/(?P<tmid>\d+)/$', detalle_us, name='detalle'),
    url(r'^detalle_us/(?P<tmid>\d+)/editar_comentario/(?P<c>\d+)$', editar_comentario, name='editar_comentario'),
    url(r'^detalle_us/(?P<tmid>\d+)/eliminar_comentario/(?P<c>\d+)$', eliminar_comentario, name='eliminar_comentario'),
    url(r'^detalle_us/(?P<usid>\d+)/historial/$', HistorialUSListView.as_view(), name='historial'),
    url(r'^detalle_us/(?P<tmid>\d+)/historial/reporte_historial$', ReporteHistorialUS.as_view(), name='reporte_historial'),
    url(r'^detalle_us/(?P<tmid>\d+)/reporte_cambio_fase$', ReporteCambioFaseUS.as_view(), name='reporte_cambio_fase'),
    url(r'^pdf/(?P<tmid>\d+)$', GeneratePDF.as_view(), name='pdf'),
    url(r'^product_backlog/$', listar_pb, name='listarPB'),
    url(r'^product_backlog/reporte$', GeneratePDF.as_view(), name='reporte'),
    url(r'^product_backlog/us_finalizados/$', listar_us_finalizados, name='finalizados'),
]