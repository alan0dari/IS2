# Generated by Django 2.0.3 on 2018-05-14 15:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('tipous', '0001_initial'),
        ('team', '0001_initial'),
        ('proyectos', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField(auto_now=True)),
                ('comentario', models.TextField()),
                ('autor', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-fecha'],
            },
        ),
        migrations.CreateModel(
            name='HistorialUS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion_corta', models.CharField(max_length=100)),
                ('descripcion_larga', models.TextField()),
                ('prioridad', models.IntegerField(default=1)),
                ('prioridad_final', models.IntegerField(default=0)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('valor_negocio', models.PositiveIntegerField()),
                ('valor_tecnico', models.PositiveIntegerField()),
                ('tiempo_estimado', models.IntegerField(default=3)),
                ('subfase', models.IntegerField(choices=[(0, 'To do'), (1, 'Doing'), (2, 'Done')])),
                ('estado', models.IntegerField(choices=[(0, 'En ejecucion'), (1, 'En espera'), (2, 'Control de calidad'), (3, 'Finalizado'), (4, 'Cancelado')])),
                ('tiempo_dedicado', models.PositiveIntegerField(default=0)),
                ('cambio_realizado', models.CharField(max_length=100, null=True)),
                ('fase_actual', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='tipous.Fase')),
                ('team_member', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='team.team_member')),
                ('tipo_us', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='tipous.TipoUS')),
            ],
            options={
                'ordering': ['fecha_modificacion'],
            },
        ),
        migrations.CreateModel(
            name='UserStory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion_corta', models.CharField(max_length=100)),
                ('descripcion_larga', models.TextField()),
                ('prioridad', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], default=1)),
                ('prioridad_final', models.IntegerField(default=0)),
                ('subfase', models.IntegerField(choices=[(0, 'To do'), (1, 'Doing'), (2, 'Done')], default=0)),
                ('estado', models.IntegerField(choices=[(0, 'En ejecucion'), (1, 'En espera'), (2, 'Control de calidad'), (3, 'Finalizado'), (4, 'Cancelado')], default=1)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('valor_negocio', models.PositiveIntegerField()),
                ('valor_tecnico', models.PositiveIntegerField()),
                ('archivo_adjunto', models.FileField(blank=True, default=None, upload_to='documents/')),
                ('tiempo_estimado', models.PositiveIntegerField(default=3)),
                ('tiempo_dedicado', models.PositiveIntegerField(default=0)),
                ('fase_actual', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='tipous.Fase')),
                ('proyecto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='proyectos.Proyecto')),
                ('team_member', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='team.team_member')),
                ('tipo_us', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='tipous.TipoUS')),
            ],
            options={
                'ordering': ['estado', '-prioridad_final', 'id'],
            },
        ),
        migrations.AddField(
            model_name='historialus',
            name='user_story',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userstory.UserStory'),
        ),
        migrations.AddField(
            model_name='comentario',
            name='us',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userstory.UserStory'),
        ),
    ]
