from django import forms
from userstory.models import UserStory


class FormUserStory(forms.ModelForm):
    class Meta:
        model = UserStory
        exclude = (
            'fase_actual',
            'estado',
            'proyecto',
            'fecha_modificacion',
            'tiempo_dedicado',
        )