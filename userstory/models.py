from django.db import models
from team.models import team_member
from proyectos.models import Proyecto
from tipous.models import TipoUS, Fase
from django.contrib.auth.models import User

subfases = ((0, 'To do'), (1, 'Doing'), (2, 'Done'))
estados = ((0, 'En ejecucion'), (1, 'En espera'), (2, 'Control de calidad'), (3, 'Finalizado'), (4, 'Cancelado'))


class UserStory(models.Model):
    """
    Clase UserStory:
    Registra un nombre, una descripcion corta, una descriocion larga, una prioridad,
    una fase, un estado, una fecha de modificacion, un valor de negocio y tecnico, un archivo adjunto,
    un tiempo estimado y dedicado para el user story y lo asocia a un proyecto, a un tipo de user story
    y a un miembro del team.
    """
    nombre = models.CharField(max_length=100)
    descripcion_corta = models.CharField(max_length=100)
    descripcion_larga = models.TextField()
    prioridad = models.IntegerField(choices=((i, i) for i in range(1, 11)), default=1)
    prioridad_final = models.IntegerField(default=0)
    fase_actual = models.ForeignKey(Fase, on_delete=models.SET_NULL, null=True)
    subfase = models.IntegerField(choices=subfases, default=0)
    estado = models.IntegerField(choices=estados, default=1)
    tipo_us = models.ForeignKey(TipoUS, on_delete=models.SET_NULL, null=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    valor_negocio = models.IntegerField(choices=((i, i) for i in range(1, 11)), default=1)
    valor_tecnico = models.IntegerField(choices=((i, i) for i in range(1, 11)), default=1)
    archivo_adjunto = models.FileField(default=None, blank=True, upload_to='documents/')
    tiempo_estimado = models.PositiveIntegerField(default=3)
    tiempo_dedicado = models.PositiveIntegerField(default=0)
    team_member = models.ForeignKey(team_member, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ['estado', "-prioridad_final","id"]

    """
    **__str__** retorna una cadena que representa el nombre del user story del proyecto en cuestion
    """
    def __str__(self):
        return self.nombre

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        query=UserStory.objects.filter(id=self.id)
        if query.count()>0:
            old=query.get()
            if(self.tiempo_dedicado != old.tiempo_dedicado):
                print("Entro")
                print(old.tiempo_dedicado)
                print(self.tiempo_dedicado)
                print(self.tiempo_dedicado)
                self.team_member.horas_trabajadas=self.team_member.horas_trabajadas +(self.tiempo_dedicado - old.tiempo_dedicado)
                print(self.team_member.horas_trabajadas)
                self.team_member.save()
        if self.tipo_us != None:
            if not self.fase_actual in self.tipo_us.fase_set.all():
                self.fase_actual= None
                print("Ndoipori Fase TUSpe")
        if self.tipo_us != None and self.fase_actual == None:
            self.fase_actual = Fase.objects.get(tipous=self.tipo_us,orden=1)
            self.subfase =  0
        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)


class HistorialUS(models.Model):

    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ['fecha_modificacion']

    user_story = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    descripcion_corta = models.CharField(max_length=100)
    descripcion_larga = models.TextField()
    prioridad = models.IntegerField(default=1)
    prioridad_final = models.IntegerField(default=0)
    tipo_us = models.ForeignKey(TipoUS, on_delete=models.SET_NULL, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    valor_negocio = models.PositiveIntegerField()
    valor_tecnico = models.PositiveIntegerField()
    tiempo_estimado = models.IntegerField(default=3)
    fase_actual = models.ForeignKey(Fase, on_delete=models.SET_NULL, null=True)
    subfase = models.IntegerField(choices=subfases)
    estado = models.IntegerField(choices=estados)
    tiempo_dedicado = models.PositiveIntegerField(default=0)
    cambio_realizado = models.CharField(max_length=100, null=True)
    team_member = models.ForeignKey(team_member, on_delete=models.SET_NULL, null=True, blank=True)


    def __str__(self):
        return self.nombre


class Comentario(models.Model):
    """
        Clase Comentario:
    Clase auxiliar que posibilita el registro de comentarios dentro de un user story.
    """
    class Meta:
        """
        **Meta-Clase**:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ["-fecha"]
    fecha = models.DateTimeField(auto_now=True)
    comentario = models.TextField()
    us = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    autor = models.ForeignKey(User, on_delete=models.DO_NOTHING)