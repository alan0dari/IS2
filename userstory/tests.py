from django.contrib.auth.models import User, Group
from django.test import TestCase

from proyectos.models import Proyecto
from team.models import team_member
from tipous.models import TipoUS
from userstory.models import UserStory

"""
Todas las pruebas de la aplicacion userstory 
Actualmente soporta las siguientes 9 pruebas:


1. **test_crear_userstory_valido** 
2. **test_crear_userstory_invalido** 
3. **test_modificar_userstory_valido**
4. **test_modificar_userstory_invalido**
5. **test_eliminar_userstory_valido**
6. **test_eliminar_userstory_invalido**
7. **test_listar_userstory_valido**
8. **test_detalles_userstory_valido**
9. **test_detalles_userstory_invalido**

**Observaciones:**

1. Correr cada test de forma independiente

"""


class test_userstory(TestCase):
    def setUp(self):
        poblar()

    def alta_developer(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/team/add"
        inscripcion = {
            "usuario": User.objects.get(username="Natalia").id,
            "rol": Group.objects.get(name="Developer").id,
            "horas_trabajo": 5,
        }
        print("Post to: " + url)
        print("Data " + str(inscripcion))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=inscripcion)
        print(respuesta.status_code)

    def registro_tipous(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/add"
        registrotipous = {
            "nombre": 'TablaPrueba',
            "descripcion":'descripcion prueba',
            "cant_fases":   2,
            "fase_1" : 'Entrevista',
            "fase_2":    'Desarrollo',
        }
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=registrotipous)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el registro de tipo de us")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la adicion de TablaPrueba")

    # === test_crear_userstory_valido ===
    def test_crear_userstory_valido(self):
        print("Inicio de test_crear_userstory_valido")
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        self.registro_tipous()
        url = "/proyecto/" + str(id_proyecto) + "/userstory/crear_us"
        crearUS = {
            "nombre": 'UserStory1',
            "descripcion_corta": 'Realización de Test',
            "descripcion_larga": 'asasdasd',
            "tipo_us": TipoUS.objects.all().get().id,
            "prioridad": 1,
            "valor_negocio": 2,
            "valor_tecnico": 3,
            "tiempo_estimado": 4,
        }
        #edu = User.objects.get(username='Eduardo')
        #id_team = team_member.objects.get(usuario=edu, proyecto_id=id_proyecto).id
        #crearUS["team_member"] = id_team
        print("Post to: " + url)
        print("Data " + str(crearUS))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=crearUS)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el registro de user story")
        she = UserStory.objects.filter(proyecto=id_proyecto)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la adicion de UserStory1s")
        print("User Story creado correctamente")

    # === test_crear_userstory_invalido ===
    def test_crear_userstory_invalido(self):
        print("Inicio de test_crear_userstory_invalido")
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        self.registro_tipous()
        url = "/proyecto/" + str(id_proyecto) + "/userstory/crear_us"
        crearUS = {
            "nombre": 'UserStory1',
            "descripcion_corta": 'Realización de Test',
            "descripcion_larga": 'asasdasd',
            "prioridad": 1,
            "tipo_us": TipoUS.objects.all().get().id,
            "valor_negocio": 2,
            "valor_tecnico": 3,
            "tiempo_estimado": 4,
            "team_member": 1,
        }
        edu = User.objects.get(username='Eduardo')
        crearUS["team_member"]=team_member(proyecto_id=id_proyecto,usuario=edu).id
        id_team = team_member.objects.get(usuario=edu.id, proyecto=id_proyecto)
        print("Post to: " + url)
        print("Data " + str(crearUS))
        self.client.logout()
        self.client.login(username="Natalia", password="adminadmin")
        respuesta = self.client.post(url, data=crearUS)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 200, msg="Exito en el registro de user story")
        she = UserStory.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 0, msg="Se ha almacenado la adicion de UserStory1")
        print("Como se espera el User Story no puede ser creado")

    # === test_modificar_userstory_valido ===
    def test_modificar_userstory_valido(self):
        print("Inicio de modificar_crear_userstory_valido")
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_us = UserStory.objects.get(proyecto=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/modificar_us/" + str(id_us) + "/"
        nuevos_datos = {
            "nombre": 'UserStory1',
            "descripcion_corta": 'Realización de Test',
            "descripcion_larga": 'Descripción editada',
            "prioridad": 1,
            "tipo_us": TipoUS.objects.all().get().id,
            "valor_negocio": 2,
            "valor_tecnico": 3,
            "tiempo_estimado": 4,
            "team_member": 1,
        }
        edu = User.objects.get(username='Eduardo')
        nuevos_datos["team_member"] = team_member.objects.get(proyecto_id=id_proyecto, usuario=edu).id
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))
        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la actualizacion ")
        she = UserStory.objects.filter(proyecto=id_proyecto)

        self.assertTrue(she.count() == 1, msg="No se ha almacenado la edicion del user story")
        print("User Story modificado correctamente")

    # === test_modificar_userstory_invalido ===
    def test_modificar_userstory_invalido(self):
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_us = UserStory.objects.get(proyecto=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/modificar_us/" + str(id_us) + "/"
        nuevos_datos = {
            "nombre": '',
            "descripcion_corta": 'Realización de Test',
            "descripcion_larga": 'asasdasd',
            "prioridad": 1,
            "valor_negocio": 2,
            "valor_tecnico": 3,
            "tiempo_estimado": 4,
            "team_member": 1,
        }
        edu = User.objects.get(username='Eduardo')
        nuevos_datos["team_member"] = team_member.objects.get(proyecto_id=id_proyecto, usuario=edu).id
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))
        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 200, msg="Éxito en la actualizacion ")
        she = UserStory.objects.filter(proyecto=id_proyecto)
        self.assertTrue(she.count() == 1, msg="Se ha almacenado la edicion del user story")
        print("Como se espera el User Story no puede ser modificado")

    # === test_eliminar_userstory_valido ===
    def test_eliminar_userstory_valido(self):
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_us = UserStory.objects.get(proyecto=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/eliminar_us/" + str(id_us) + "/"
        print("Post to: " + url)
        respuesta = self.client.post(url)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el borrado")
        she = UserStory.objects.filter(proyecto=id_proyecto)
        self.assertTrue(she.count() == 0, msg="No se ha borrado el user story")
        print("User Story eliminado correctamente")

    # === test_eliminar_userstory_invalido ===
    def test_eliminar_userstory_invalido(self):
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "proyecto/1/userstory/eliminar_us/23"
        print("Post to: " + url)
        self.client.post(url)
        she = UserStory.objects.filter(proyecto=id_proyecto)
        self.assertTrue(she.count() == 1, msg="Se ha borrado el user story")
        print("Como se espera el user story no puede ser eliminado porque no existe")

    # === test_listar_userstory ===
    def test_listar_userstory(self):
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/"
        print("Post to:" + url)
        respuesta = self.client.get(url)
        self.assertTrue(respuesta.status_code == 200, msg="No se puede mostrar el listado de user stories")
        print("Se muestra correctamente la lista de user stories")

    # === test_detalles_userstory_valido ===
    def test_detalles_userstory_valido(self):
        self.test_crear_userstory_valido()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_us = UserStory.objects.get(proyecto=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/detalle_us/" + str(id_us) + "/"
        print("Post to:" + url)
        respuesta = self.client.get(url)
        self.assertTrue(respuesta.status_code == 200, msg="No se puede mostrar los detalles del user story")
        print("Se muestra correctamente los detalles del user story")

    # === test_detalles_userstory_invalido ===
    def test_detalles_userstory_invalido(self):
        self.test_crear_userstory_valido()
        url = "proyecto/1/userstory/detalle_us/88/"
        print("Post to:" + url)
        respuesta = self.client.get(url)
        self.assertTrue(respuesta.status_code == 404, msg="Se puede mostrar los detalles del user story")
        print("Como se espera no se muestran los detalles del user story porque no existe")

    # === test_xreporte ===
    def test_xreporte(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/userstory/product_backlog/" +"reporte"
        print("Post to:" + url)
        respuesta=self.client.get(url)
        print(respuesta)
        self.assertTrue(respuesta.status_code == 200,msg='Se puede mostrar reporte')







from django.contrib.auth.models import User, Permission, Group
from team.models import team_member
from proyectos.models import Proyecto

def poblar():
    perm_scrum=['add_Integrante','edit_Integrante','delete_Integrante',
                'view_proyecto','change_proyecto','delete_proyecto','ver_ProductBacklog',
                'ver_UserStory', 'crear_UserStory', 'modificar_UserStory', 'eliminar_UserStory',
                'add_Tus','edit_Tus','delete_Tus','fases_Tus']

    usuarios=["Alan","Eduardo","Guillermo","Natalia"]
    roles=["Scrum Master","Developer","Tester"]
    proyectos=["SGP","ATM","Download Tester"]
    new_users = {}
    new_roles = {}
    new_proyectos = {}
    new_sys_roles= {}
    for i in ["Administrador","Usuario Normal"]:
        query=Group.objects.filter(name=i)
        if query.count()>0:
            new_sys_roles[i]=query.get()
        else:
            new_sys_roles[i]=Group.objects.create(name=i)
    new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename='view_proyecto'))
    new_sys_roles["Usuario Normal"].permissions.add(Permission.objects.get(codename='add_proyecto'))
    for i in ["add","change","delete"]:
        for j in ["group","user"]:
            new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename=i+'_'+j))

    for i in usuarios:
        try:
            query= User.objects.filter(username=i)
            if query.count()==1 :
                new_users[i]=query.get()
            else:
                new_users[i]=User.objects.create_user(username=i,password="adminadmin")
        except:
            if User.objects.filter(username=i).count()==0:
                print("No se puedo crear el usuario :"+i)
        if i == "Guillermo":
            new_users[i].groups.add(new_sys_roles["Administrador"])
        else:
            new_users[i].groups.add(new_sys_roles["Usuario Normal"])
    for i in roles:
        try:
            query=Group.objects.filter(name=i)
            if query.count()==1:
                new_roles[i]=query.get()
            else:
                new_roles[i]=Group.objects.create(name=i)
        except:
            print("No se pudo crear rol")

    for i in perm_scrum:
        try:
            grupo = Group.objects.get(name="Scrum Master")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

    for i in proyectos:
            if Proyecto.objects.filter(nombre=i).count() == 0 :
                new_proyectos[i]=Proyecto.objects.create(nombre=i,descripcion="")
                team_member.objects.create(usuario=new_users["Eduardo"],
                                           rol=new_roles["Scrum Master"],
                                           proyecto=new_proyectos[i])

    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="add_group"))
    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="change_group"))
