import datetime

from django.contrib.admin.models import ADDITION, CHANGE, DELETION
import datetime

from django.contrib.admin.models import DELETION, ADDITION, CHANGE
from django.contrib.auth.models import Group, User
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from proyectos.models import Proyecto, Auditoria
from sprint.models import Sprint
from proyectos.views import log
from sgp.utils import render_to_pdf
from sprint.models import Sprint
from team.models import team_member
from tipous.models import TipoUS, Fase
from userstory.models import UserStory, Comentario
from userstory.models import UserStory, HistorialUS
from guardian.mixins import PermissionRequiredMixin




# Las pruebas unitarias de las vistas se encuentran en [[tests.py]]
# Los modelos se encuentran en [[models.py]]

# === crear_us ===
def crear_us(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un formulario para la creación de user story en el proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.crear_UserStory')
    has_permission |= request.user.has_perm('proyectos.crear_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return crearUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === listar_us ===
def listar_us(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con un listado de los user stories del proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        #return redirect("/proyecto/"+kwargs['pk']+"/userstory/product_backlog/")
        return listarUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === modificar_us ===
def modificar_us(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un formulario para la modificación del user story, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.modificar_UserStory')
    has_permission |= request.user.has_perm('proyectos.modificar_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return modificarUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === eliminar_us ===
def eliminar_us(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un pedido de confirmacion para la eliminación del user story, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.eliminar_UserStory')
    has_permission |= request.user.has_perm('proyectos.eliminar_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return eliminarUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === detalle_us ===
def detalle_us(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con los detalles del user story seleccionado, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return detalleUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === crearUS ===
class crearUS(CreateView):
    """
    Creación de User Story.
    **Hereda de:**

    1. CreateView: Vista para la creacion de una nueva instancia de un
    objeto con una respuesta renderizada por un template

    **Retorna:** url a listarUS

    """
    model = UserStory
    fields = [
        'nombre', 'descripcion_corta',
        'descripcion_larga', 'prioridad',
        'tipo_us',
        'valor_negocio', 'valor_tecnico',
        'archivo_adjunto', 'tiempo_estimado'
    ]
    template_name = "userstory/crear_us.html"

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega al nuevo user story un indicador del proyecto y del miembro del team
        al cual esta siendo asignado, además de realizar el envío de email con los detalles sobre el
        User story al miembro asignado y se calcula el valor de la prioridad del user story
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.proyecto = Proyecto.objects.get(id=kwargs['pk'])
        if form.is_valid():
            sendEmail(form,kwargs)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de miembros de team,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(CreateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        team_list = team_member.objects.filter(proyecto=context['proyecto'])
        tus_list = TipoUS.objects.filter(proyecto=context['proyecto'])
        user_list = User.objects.all()

        for member in team_list:
            user_list = user_list.exclude(id=member.usuario.id)

        user_list = user_list.exclude(username="AnonymousUser")
        context['user_list'] = user_list
        context['team_list'] = team_list
        context['tus_list'] = tus_list

        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.object.proyecto.id
        log(self,ADDITION,'Creacion de User Story')
        return reverse_lazy("proyectos:userstory:listar", kwargs={'pk': l})


# === listarUS ===
class listarUS(ListView):
    """
    Menu de listado de user stories del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,seteado por
    `self.model` or `self.queryset`.`self.queryset` puede ser cualquier
    iterable de items no solo un queryset

    **Retorna:** url a listarUS
     """
    # `Extends:` ListView; para la visualizacion de los user stories.
    model = UserStory
    template_name = 'userstory/listar_us.html'
    paginate_by = 6

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return UserStory.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context


# === modificarUS ===
class modificarUS(UpdateView):
    """
    Menú para la modificación de user story
    **Hereda de:**

    1. UpdateView: Vista para modificar un objeto,con una respuesta renderizada por un template

    **Retorna**: url a listarUS
     """
    # `Extends:` EditView; para la visualizacion del menu de modificación.
    model = UserStory
    fields = [
        'nombre', 'descripcion_corta', 'descripcion_larga',
        'prioridad', 'valor_negocio', 'valor_tecnico', 'tipo_us',
        'archivo_adjunto', 'tiempo_estimado',
    ]
    template_name = 'userstory/modificar_us.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del user story a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,CHANGE,'Edicion de User Story')
        return reverse_lazy("proyectos:userstory:listar",kwargs = {'pk': l})

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de miembros de team,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        team_list = team_member.objects.filter(proyecto=context['proyecto'])
        tus_list = TipoUS.objects.filter(proyecto=context['proyecto'])
        user_list = User.objects.all()

        for member in team_list:
            user_list = user_list.exclude(id=member.usuario.id)

        user_list = user_list.exclude(username="AnonymousUser")
        context['user_list'] = user_list
        context['team_list'] = team_list
        context['tus_list'] = tus_list

        return context


    def form_valid(self, form):
        us = UserStory.objects.get(id=self.kwargs["tmid"])
        cambio = "Edición de campos"
        autor = us.team_member
        us.historialus_set.create(nombre=us.nombre, descripcion_corta=us.descripcion_corta,
                                  descripcion_larga=us.descripcion_larga, prioridad=us.prioridad,
                                  prioridad_final=us.prioridad_final, tiempo_estimado=us.tiempo_estimado,
                                  tipo_us=us.tipo_us, valor_negocio=us.valor_negocio, cambio_realizado=cambio,
                                  valor_tecnico=us.valor_tecnico, fecha_modificacion=us.fecha_modificacion,tiempo_dedicado=us.tiempo_dedicado
                                  ,fase_actual=us.fase_actual,subfase=us.subfase,estado=us.estado, team_member=autor)
        self.object = form.save()
        sendEmail(form,self.kwargs)
        return super(modificarUS, self).form_valid(form)


# === eliminarUS ===
class eliminarUS(DeleteView):
    """
    Solicitud de confirmacion para la eliminación de un user story del proyecto.
    **Hereda de:**

    1. DeleteView: Vista para borrar un objeto recolectado con  `self.get_object()`,
        con una respuesta renderizada con una template

    **Retorna:** url a listarUS

    """
    # `Extends:` DeleteView; para visualizacion de la conformacion de expulsión.
    model = UserStory
    template_name = 'userstory/eliminar_us.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del user story a ser eliminado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs['pk']
        log(self,DELETION,'Borrado de un User Story')
        return reverse_lazy("proyectos:userstory:listar", kwargs={'pk': l})


# === detalleUS ===
class detalleUS(DetailView):
    """
    **Hereda de:**

    1.DetailView: por defecto esta es una instancia
        de modelo para `self.queryset` pero
        la vista soporta mostrar cualquier
        objeto si se sobreescribe `self.get_object()

    **Retorna:** url a listarUS
    """
    model = UserStory
    template_name = "userstory/detalle_us.html"

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return UserStory.objects.filter(proyecto=self.proyecto)

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del user story a ser visualizado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(DetailView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        comment_list = Comentario.objects.filter(us=self.kwargs['tmid'])
        context['comment_list'] = comment_list
        context['usuario'] = self.request.user.username
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs['pk']
        return reverse_lazy("proyectos:userstory:listar", kwargs={'pk': l})


class HistorialUSListView(PermissionRequiredMixin, ListView):
    model = HistorialUS
    permission_required = "proyectos.ver_UserStory"
    template_name = "userstory/historial.html"
    paginate_by = 8
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_queryset(self):
        """
        `Override:` personaliza la lista de versiones pasadas del User Story.
        """
        self.user_story = get_object_or_404(UserStory, id=self.kwargs['usid'])
        return HistorialUS.objects.filter(user_story=self.user_story)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['user_story'] = UserStory.objects.get(id=self.kwargs['usid'])
        return context


def sendEmail(form,kwargs):
    try:
        nombre = form.cleaned_data['nombre']
        descripcion = form.cleaned_data['descripcion_corta']
        prioridad = form.cleaned_data['prioridad']
        tus_nombre = form.cleaned_data['tipo_us']
        tus = TipoUS.objects.get(nombre=tus_nombre, proyecto=Proyecto.objects.get(id=kwargs['pk']).id)
        # print(tus.nombre)
        fases = Fase.objects.filter(tipous=tus.id)
        # print("Fase: ",fases.first())
        form.instance.fase_actual = fases.first()
        valor_tecnico = form.cleaned_data['valor_tecnico']
        valor_negocio = form.cleaned_data['valor_negocio']
        calculus = int(int(int(prioridad) + int(valor_negocio) + int(valor_tecnico)) / 3)
        form.instance.prioridad_final = calculus

        pro = Proyecto.objects.get(id=kwargs['pk'])
        member_name = form.cleaned_data['team_member']
        query = User.objects.filter(username=member_name)
        if query.count() > 0 :
            usuario = query.get()
            member_asignado = team_member.objects.get(usuario=usuario.id, proyecto=form.instance.proyecto)
            email = member_asignado.usuario.email
            html_content = "Le ha sido asignado el User Story %s en el proyecto: %s<br> Descripción del US: %s <br>Prioridad: %s<br>"
            message = EmailMessage(subject='Asignación de User Story',
                                   body=html_content % (nombre, pro.nombre, descripcion, prioridad), to=[email])
            message.content_subtype = 'html'
            message.send()
    except :
        print("No se envio el correo de notificacion ")


# === listar_pb ===
def listar_pb(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    Se muestra el Product Backlog con un listado de los user stories del proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    has_permission = request.user.has_perm('proyectos.ver_ProductBacklog')
    has_permission |= request.user.has_perm('proyectos.ver_ProductBacklog', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return listarPB.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === listar_us_finalizados ===
def listar_us_finalizados(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con un listado de los user stories finalizados del proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyectos.ver_UserStory')
    has_permission |= request.user.has_perm('proyectos.ver_UserStory', Proyecto.objects.get(id=kwargs['pk']))
    has_permission = request.user.has_perm('proyectos.ver_USfinalizados')
    has_permission |= request.user.has_perm('proyectos.ver_USfinalizados', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return listarUSfinalizados.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === listarPB ===
class listarPB(ListView):
    """
    Menu de listado de user stories del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,seteado por
    `self.model` or `self.queryset`.`self.queryset` puede ser cualquier
    iterable de items no solo un queryset

    **Retorna:** url a proyecto
     """
    # `Extends:` ListView; para la visualizacion de los user stories.
    model = UserStory
    template_name = 'userstory/product_backlog.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return UserStory.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        us_list = UserStory.objects.filter(proyecto=context['proyecto'])
        tus_list = TipoUS.objects.filter(proyecto=context['proyecto'])
        context['tus_list'] = tus_list
        context['us_list'] = us_list
        return context


# === listarUSfinalizados ===
class listarUSfinalizados(ListView):
    """
    Menu de listado de user stories finalizados del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,seteado por
    `self.model` or `self.queryset`.`self.queryset` puede ser cualquier
    iterable de items no solo un queryset

    **Retorna:** url a proyecto
     """
    # `Extends:` ListView; para la visualizacion de los user stories.
    model = UserStory
    template_name = 'userstory/us_finalizados.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return UserStory.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        us_list = UserStory.objects.filter(proyecto=context['proyecto'], estado=3)
        context['us_list'] = us_list
        return context


# === detalleUS ===
class DetalleTeamUS(DetailView):
    """
    **Hereda de:**

    1.DetailView: por defecto esta es una instancia
        de modelo para `self.queryset` pero
        la vista soporta mostrar cualquier
        objeto si se sobreescribe `self.get_object()

    **Retorna:** url a listarUS
    """
    model = UserStory
    template_name = "userstory/detalle_us.html"
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return UserStory.objects.filter(proyecto=self.proyecto)

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del user story a ser visualizado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['usid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(DetailView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs['pk']
        return reverse_lazy("proyectos:userstory:listar", kwargs={'pk': l})



# === GeneratePDF ===
class GeneratePDF(View):
    permission_required = 'proyectos.ver_ProductBacklog'
    raise_exception = True
    template_name = 'pdf/reporteEnProyecto.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
                `Override:` Agrega el contexto y genera el pdf.
                """
        template = get_template('pdf/reporteEnProyecto.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        context = {
            "proyecto": Proyecto.objects.get(id=self.kwargs['pk']),
            "hora": datetime.datetime.now(),
            "us_list": UserStory.objects.filter(proyecto=Proyecto.objects.get(id=self.kwargs['pk'])),
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteEnProyecto.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("321456")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")


# === ReporteCambioFaseUS ===
class ReporteCambioFaseUS(View):
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteCambioFaseUS.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteCambioFaseUS.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        context = {
            "proyecto": Proyecto.objects.get(id=self.kwargs['pk']),
            "hora": datetime.datetime.now(),
            "user_story": UserStory.objects.get(id=self.kwargs['tmid']),
            "log": Auditoria.objects.filter(proyecto=Proyecto.objects.get(id=self.kwargs['pk'])),
            "historial": HistorialUS.objects.filter(user_story=self.kwargs['tmid'],cambio_realizado="Cambio de Fase"),
        }
        print(context["historial"])
        if context["historial"].count() == 0 :
            context["historial"] = None
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteCambioFaseUS.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("321456")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")


# === ReporteHistorialUS ===
class ReporteHistorialUS(View):
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteHistorialUS.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteHistorialUS.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        context = {
            "proyecto": Proyecto.objects.get(id=self.kwargs['pk']),
            "hora": datetime.datetime.now(),
            "user_story": UserStory.objects.get(id=self.kwargs['tmid']),
            "log": Auditoria.objects.filter(proyecto=Proyecto.objects.get(id=self.kwargs['pk'])),
            "historial": HistorialUS.objects.filter(user_story=self.kwargs['tmid']),
        }

        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteHistorialUS.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("321456")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

