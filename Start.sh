#! /bin/bash

#Nota: Verifique que los datos de la "cabecera" estan debidamente seteados.

#Nota: Script optimista, asume que los programas a continuación estan debidamente instalados y configurados.
    # virtualenv
    # postgresql (Version 10.3 de preferencia)
    # python3-pip
    # apache2
    # libapache2-mod-wsgi-py3
    # git
    # sudo apt-get install python3-tk
###### CABECERA ######

#Nota: La mayoría de las direcciones surgen a partir del Home, se expresan los casos en que no.

###Variables###

# Opciones de Instalación (1: True; 0 False)

#inst_apache: setea el archivo de configuracion del apache y reinicia el proceso
inst_apache=0
#Entorno a desplegar
display_desarrollo=1
display_produccion=1

# Git
#git_tag: Branch o Tag del cual descargar
git_tag="5b8d80ecc602b95f0211c51be6ae318593f645bf"
#git_repositorio: URL del repositoro remoto
git_repositorio="https://gitlab.com/alan0dari/IS2.git"


# Sistema

#user: Nombre del usuario
user="guille"

# Proyecto

#dir_project: A partir de home y hasta antes de una carpeta del proyecto, aqui deben estar las versiones de produccion y desarrollo
dir_project="PycharmProjects"
#project_name: Nombre de la carpeta de la version de desarrollo del proyecto
project_name="Desarrollo"
#production: Nombre de la carpeta de la version de produccion del proyecto
production_name="Produccion"
#app_name: Nombre de la aplicacion Principal
app_name="sgp"


# Virtualenv

#env_dir: Nombre de la carpeta del virtualenv
env_dir="venvIS2"

# Apache

#apache_dir: directorio en que se encuentra la configuracion adicional del Apache (directorio absoluto)
apache_dir="/etc/apache2/sites-available"
#apache_conf: archivo en que se encuentra la configuracion adicional del Apache (directorio relativo al apache_dir)
apache_conf="000-default.conf"

# Postgres

#db_dev: Base de datos del desarrollo
db_dev="sgpdev"
#db_prod: Base de datps de produccion
db_prod="sgpprod"


###### FIN DE LA CABECERA ######

###### CUERPO ######

###Varables autocalculadas####

home="/home/"$user
project_abs=$home"/"$dir_project"/"$project_name
produccion_abs=$home"/"$dir_project"/"$production_name
env_abs=$home"/"$env_dir
apache_conf_dir=$apache_dir"/"$apache_conf


###Script###

#Montar VirtualEnv

echo -e "\nPaso previo 1/2: Virtualenv\n"

inst_venv=0
if [ $inst_venv -eq 1 ]; then
	if [ ! -f $env_abs"/bin/activate" ] ; then
		virtualenv -p python3 $home"/"$env_dir
        echo "Se creo un nuevo entorno virtual en $env_abs"
    else
        echo "Ya existe entorno virtual en $env_abs"
	fi
fi

source $env_abs/bin/activate

if [ -f "venvinstall" ] ; then
    echo "Actualizando contenido del Virtualenv"
	pip install -r venvinstall
    echo "Contenido del Virtualenv actualizado"
else
    echo "No se encontro archivo de configuracion del virtualenv "
fi

deactivate

#Montar Apache

echo -e "\nPaso previo: Configuracion del Apache\n"

if [ $inst_apache -eq 1 ]; then

	apache_000_config="
	Alias /static $produccion_abs/static
	<Directory $produccion_abs/static>
		Require all granted
	</Directory>
	<Directory $produccion_abs>
		<Files wsgi.py>
			Require all granted
		</Files>
	</Directory>

	WSGIDaemonProcess $project_name python-home=$env_abs python-path=$produccion_abs
	WSGIProcessGroup $project_name
	WSGIScriptAlias / $produccion_abs/$app_name/wsgi.py
"
	echo "Configurando archivo $apache_conf_dir"

	sudo sh -c "echo \"<VirtualHost *:80>$apache_000_config</VirtualHost>\"  > $apache_conf_dir"

	a2enmod wsgi
	a2ensite $apache_conf
	/etc/init.d/apache2 restart
    echo "Servicio reinstalado"
else
    echo "Paso omitido"
fi

#Funcion de despliegue
#   $1 Nombre del entorno
#   $2 Directorio absoluto
#   $3 Base de Datos
#   $4 Numero identificador (0:desarrollo, 1:produccion)

display (){
    echo -e "\nDesplegar entorno de $1\n"
    echo "Nota: Se asume que en \"$directorio\" esta el manage.py y el Backup.json"

    if [ -d $2 ]; then
        cd $2
        for i in $(git tag); do
            if [ $i == $git_tag ]; then
                prefijo="tags/"
            fi
        done
        git checkout $prefijo$git_tag -f
        git pull
    else
        git clone -b $git_tag $git_repositorio $2
        cd $2
    fi
    if [ $4 -eq 1 ]; then
        echo "Reemplazo del archivo settings.py"
        cp -f $2"/sgp/settings_prod.py" $2"/sgp/settings.py"

    fi
    echo "Borrando $3"
    sudo -u postgres psql -c "DROP DATABASE if exists $3;"
    echo "Borrado de $3 finalizado"
    echo "Creando $3"
    sudo -u postgres psql -c "CREATE DATABASE $3;"
    echo "Creacion de $3 finalizada"
    for i in $(ls $2); do
        migrate=$2"/"$i"/migrations"
        if [ -d $migrate ] ; then
	    cd $i
            pycco *.py
	    cd ..
            echo "Borrar migraciones anteriores: $migrate"
	        rm -rf $migrate
        fi

	
    done
    source $env_abs/bin/activate
    for app in sgp proyectos team tipous userstory sprint ; do
        python manage.py makemigrations $app
    done
    python manage.py migrate
    python manage.py loaddata Backup


    if [ $4 -eq 1 ]; then
        sudo /etc/init.d/apache2 restart
    else
	    ##python manage.py test
        python manage.py runserver 8000
    fi
    deactivate
}


if [ $display_desarrollo -eq 1 ]; then
    display "Desarrollo" $project_abs $db_dev 0
fi

if [ $display_produccion -eq 1 ]; then
    display "Produccion" $produccion_abs $db_prod 1
fi
