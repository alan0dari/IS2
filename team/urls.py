from django.urls import path,include
from django.conf.urls import url


from .views import agregar,editar,expulsar,listar

app_name = 'team'
urlpatterns = [
    url(r'^$', listar, name='list'),
    url(r'^add$', agregar, name='Agregar'),
    url(r'^kick/(?P<tmid>\d+)/$', expulsar, name='kick'),
    url(r'^edit/(?P<tmid>\d+)/$', editar, name='editar'),
]