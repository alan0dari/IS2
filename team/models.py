from django.contrib.auth.models import User,Group,Permission
from django.db import models
from guardian.shortcuts import assign_perm, remove_perm, get_perms

from proyectos.models import Proyecto

# === Modelos para team ===
class team_member(models.Model):
    """
        Clase team_member:
    Asocia a cada usuario con los proyectos en los cuales participa, y
    con el rol que desempeña dentro de esos proyectos. Además, registra
    las horas diarias disponibles del usuario para dicho proyecto.
    """
    class Meta:
        """
        **Meta-Clase**:
        Define la unicidad de la relacion Usuario-Proyecto, y establece los
        parametros de ordenamiento de los objetos de la clase.
        """
        unique_together = (('usuario', 'proyecto'),)
        ordering = ["rol","proyecto","usuario"]
    usuario = models.ForeignKey(User,on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto,on_delete=models.CASCADE)
    rol = models.ForeignKey(Group,on_delete=models.SET_NULL,null=True)
    horas_trabajo= models.IntegerField(default=4)
    horas_trabajadas=models.IntegerField(default=0)
    def __str__ (self):
        """
        `Override:` personaliza la representación en formato "cadena" del objeto.
        """
        try:
            usuario=self.usuario.username
        except:
            usuario=""
        try:
            rol_asignado=self.rol.name
        except :
            rol_asignado="?"
        try:
            proyecto=self.proyecto.nombre
        except :
            proyecto_in=""

        return  "%s" %(usuario)

    def save(self, force_insert=False, force_update=False, using=None,update_fields=None):
        """
        `Override:` dada la alta o modificación de un team member, otorga los permisos
        correspondientes al rol asignado al usuario integrante; y  quita los permisos
        que poseía anteriormente sobre dicho objeto.
        """
        old = team_member.objects.filter(id=self.id)
        if old.count()> 0 :
            for x in get_perms(self.usuario,self.proyecto):
                print("Remover " + x + " a " + self.usuario.username + " en " + self.proyecto.nombre)
                remove_perm(x,self.usuario,self.proyecto)
        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)
        rol=self.rol
        if rol == None:
            x=Permission.objects.get(codename="view_proyecto")
            print("Conceder " + x.codename + " a " + self.usuario.username + " en " + self.proyecto.nombre)
            assign_perm(x.codename, self.usuario, self.proyecto)
        else:
            for x in rol.permissions.all():
                if x.content_type.name == 'proyecto':
                    print("Conceder " + x.codename + " a "+self.usuario.username+" en "+self.proyecto.nombre)
                    assign_perm(x.codename,self.usuario,self.proyecto)


    def delete(self, *args, **kwargs):
        """
        `Override:` quita los permisos del usuario sobre el proyecto del cual
        ha sido expulsado.
        """
        old = team_member.objects.filter(id=self.id)
        if old.count() > 0:
            for x in get_perms(self.usuario, self.proyecto):
                print("Remover " + x + " a " + self.usuario.username + " en " + self.proyecto.nombre)
                remove_perm(x, self.usuario, self.proyecto)
        super().delete(*args, **kwargs)