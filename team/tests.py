from django.test import TestCase
from django.contrib.auth.models import User, Group
from team.models import team_member
from proyectos.models import Proyecto
from poblar import poblar

class test_Team(TestCase):
    # `Override:` genera los datos de prueba.
    def setUp(self):
        poblar()

    def test_verificar_cantidad(self):
        #Verifica que cada proyecto tenga un integrante
        for i in Proyecto.objects.all():
            list_miembros=team_member.objects.filter(proyecto=i)
            self.assertTrue(list_miembros.count()>0,msg="El proyecto " + i.nombre + " no tiene integrantes")

    def test_adicion(self):
        #Intenta agregar un integrante al team
        id_proyecto=Proyecto.objects.get(nombre="SGP").id
        url="/proyecto/"+str(id_proyecto)+"/team/add"
        inscripcion={
            "usuario":User.objects.get(username="Natalia").id,
            "rol":Group.objects.get(name="Scrum Master").id,
            "horas_trabajo":5,
        }
        print("Post to: "+ url)
        print("Data "+str(inscripcion))
        self.client.login(username="Eduardo",password="adminadmin")
        respuesta=self.client.post(url,data=inscripcion)
        self.assertTrue(respuesta.status_code==302,msg="Error en la inscripcion")
        she=team_member.objects.filter(usuario_id=inscripcion["usuario"],
                                       proyecto_id=id_proyecto)
        self.assertTrue(she.count()==1,msg="No se ha almacenado la adicion de Natalia a SGP")

    def test_adicion_2(self):
        #Se intenta agregar integrantes sin poseer permisos
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/team/add"
        inscripcion = {
            "usuario": User.objects.get(username="Natalia").id,
            "rol": Group.objects.get(name="Scrum Master").id,
            "horas_trabajo": 5,
        }
        print("Post to: " + url)
        print("Data " + str(inscripcion))
        self.client.login(username="Guillermo", password="adminadmin")
        self.client.post(url, data=inscripcion)
        she = team_member.objects.filter(usuario_id=inscripcion["usuario"],
                                         proyecto_id=id_proyecto)
        self.assertFalse(she.count() == 1,
                         msg="Un usuario ajeno al Team logro agregar un usuario al mismo")
        self.test_editar()
        inscripcion["usuario"]=User.objects.get(username="Alan").id
        self.client.logout()
        self.client.login(username="Natalia", password="adminadmin")
        self.client.post(url, data=inscripcion)
        he = team_member.objects.filter(usuario_id=inscripcion["usuario"],
                                        proyecto_id=id_proyecto)
        self.assertFalse(he.count() == 1,
                         msg="Un integrante sin permisos logro añadir un usuario al Team")

    def test_editar(self):
        #Se edita el rol de un integrante
        self.test_adicion()
        id_Nati= User.objects.get(username="Natalia").id
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_member = team_member.objects.get(usuario_id=id_Nati,
                                            proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/team/edit/"+str(id_member)+"/"
        nuevos_datos = {
            "rol": Group.objects.get(name="Tester").id,
            "horas_trabajo": 5,
        }
        respuesta = self.client.post(url, data=nuevos_datos)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la actualizacion ")
        she = team_member.objects.get(id=id_member)
        self.assertTrue(she.rol_id==nuevos_datos["rol"],
                        msg="No se ha almacenado la edicion de Natalia en SGP")

    def test_expulsar(self):
        #Se expulsa a un integrante
        self.test_adicion()
        id_Nati = User.objects.get(username="Natalia").id
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_member = team_member.objects.get(usuario_id=id_Nati,
                                            proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/team/kick/" + str(id_member) + "/"
        respuesta = self.client.post(url)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la expulsion")
        she = team_member.objects.filter(id=id_member)
        self.assertTrue(she.count() == 0,msg="No se ha expulsado a Natalia de SGP")
