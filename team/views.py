from django.contrib.admin.models import ADDITION, DELETION, CHANGE
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User,Group
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import CreateView,ListView,DeleteView,UpdateView

from proyectos.views import log
from team.models import team_member
from proyectos.models import Proyecto

#=== agregar ===
def agregar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la adicion de integrantes al equipo, o un
    mensaje de alerta si no los posee.
    """
    #`Return:` HttpResponse de acuerdo al caso.
    if has_perm(request.user,'proyectos.add_Integrante',kwargs['pk']):
        return addIntegrante.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== listar ===
def listar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con un listado de los integrantes al equipo, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    if has_perm(request.user,'proyectos.view_proyecto',kwargs['pk']):
        return listIntegrante.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== editar ===
def editar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la edicion de los atributos de los integrantes al equipo, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(team_member.objects.get(id=kwargs['tmid']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyectos:team:editar", *args, **kwargs)
    if has_perm(request.user,'proyectos.edit_Integrante',kwargs['pk']):
        return editIntegrante.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== expulsar ===
def expulsar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un pedido de confirmacion de la expulsion del integrante del equipo, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id= str(team_member.objects.get(id=kwargs['tmid']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk']=proy_id
        return redirect("proyectos:team:kick",*args,**kwargs)
    if has_perm(request.user,'proyectos.delete_Integrante',kwargs['pk']):
        return kickIntegrante.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== addIntegrante ===
class addIntegrante(CreateView):
    """
    Menu de adicion de miembros al Equipo.
    """
    #`Extends:` CreateView; para la visualizacion del menu de adicion de miembros al Equipo.
    model = team_member
    fields = ['usuario','rol','horas_trabajo']
    template_name = "team/add.html"

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega al nuevo integrante el indicador del proyecto
        al cual esta siendo inscripto.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.proyecto=Proyecto.objects.get(id=kwargs['pk'])
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de usuarios y roles disponibles,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(CreateView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        team=team_member.objects.filter(proyecto=context['proyecto'])
        user_list=User.objects.all()
        for member in team:
            user_list=user_list.exclude(id=member.usuario.id)
        user_list=user_list.exclude(username="AnonymousUser")
        for admin in User.objects.filter(groups__name="Administrador"):
            user_list = user_list.exclude(id=admin.id)
        context['user_list']=user_list
        context['rol_list']=Group.objects.all().exclude(name="Administrador").exclude(name = "Usuario Normal")
        return context

    def get_success_url(self):
        #`Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.object.proyecto.id
        log(self,ADDITION,'Nuevo Team Member')
        return reverse_lazy("proyectos:team:list",kwargs = {'pk': l})

#=== listIntegrante ===
class listIntegrante(ListView):
    """
    Menu de listado miembros del Equipo.
    """
    # `Extends:` ListView; para la visualizacion del menu.
    model = team_member
    template_name = 'team/listar.html'
    paginate_by = 5

    def get_queryset(self):
        """
        `Override:` personaliza la lista de usuarios a fin de proyectar solo los
        pertenecientes al equipo del proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return team_member.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        for i in ['add','edit','delete']:
            context['perm_'+i]=has_perm(self.request.user,"proyectos."+i+"_Integrante",self.kwargs["pk"])
        return context

#=== kickIntegrante===
class kickIntegrante(DeleteView):
    """
    Solicitud de confirmacion de expulsion de un miembro del Equipo.
    """
    # `Extends:` DeleteView; para visualizacion de la confirmacion de expulsión.
    model = team_member
    template_name = 'team/borrar.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del miembro a ser expulsado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,DELETION,'Expulsion de Team Member')
        return reverse_lazy("proyectos:team:list",kwargs = {'pk': l})

#=== editIntegrante ==
class editIntegrante(UpdateView):
    """
    Menu de edicion de caracteristicas de un miembro del Equipo.
    """
    # `Extends:` EditView; para la visualizacion del menu de edicion.
    model = team_member
    fields = ['rol', 'horas_trabajo']
    template_name = 'team/editar.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del miembro a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,CHANGE,'Edicion de Team Member ')
        return reverse_lazy("proyectos:team:list",kwargs = {'pk': l})

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(UpdateView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        context['rol_list']=Group.objects.all().exclude(name="Administrador").exclude(name = "Usuario Normal")
        return context

# === has_perm ===
def has_perm(user,perm,proy_id):
    #Funcion auxiliar para verificar que el usuario `user` tenga el permiso `perm`
    #en el proyecto de id `proy_id`.
    """
    `Return:` True si posee permisos, False si no.
    """
    has = user.has_perm(perm, Proyecto.objects.get(id=proy_id))
    print(perm+"? "+str(has))
    return has
