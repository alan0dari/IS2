from django.contrib.auth.models import User
from django.test import TestCase
from .forms import LoginForm
from django.contrib.auth import authenticate
from django.urls import reverse


"""
Todas las pruebas de la aplicacion sgp 
Actualmente soporta las siguientes 3 pruebas:


1. **Test_Conectar** 
2. **test_logout** 

"""

# === clase Test_Conectar ===
class Test_Conectar(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(username='Pepe',email='pepe@gmail.com',password='adminadminadmin')
    def test_login_correcto(self):
        # === test_login_correcto===
        form = LoginForm(data={'nombre': 'Pepe', 'password': 'adminadminadmin'})
        self.assertTrue(form.is_valid())
        user_auth = authenticate(username='Pepe', password='adminadminadmin')
        self.assertTrue(user_auth)
        user_login = self.client.login(username="Pepe", password="adminadminadmin")
        self.assertTrue(user_login)
        response = self.client.get(reverse('principal'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'base.html')

    def test_login_incorrecto(self):
        # === test_login_incorrecto===
        form = LoginForm(data={'nombre': 'Pepe', 'password': ''})
        self.assertFalse(form.is_valid())
        user_auth = authenticate(username='Pepe', password='')
        self.assertFalse(user_auth)
        user_login = self.client.login(username="Pepea", password="")
        self.assertFalse(user_login)
        response = self.client.get(reverse('principal'))
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        # === test_logout===
        self.client.login(username="Pepe", password="adminadminadmin")
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)



















