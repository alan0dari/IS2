"""sgp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, \
    password_reset_complete
from django.urls import path
from django.conf.urls import url, include
from .views import PaginaPrincipalView, conectar, desconectar, Historial

app_name = 'sgp'
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^principal$',PaginaPrincipalView.as_view(), name='principal'),
    url(r'^historial$',Historial.as_view(), name='historialSistema'),
    url(r'^login', conectar, name='login'),
    url(r'^logout', desconectar, name='logout'),
    url(r'^roles/', include(('roles.urls','roles'), namespace='roles')),
    url(r'^proyecto/', include(('proyectos.urls'), namespace='proyectos')),
    url(r'^usuarios/', include("usuarios.urls", namespace='usuarios')),
    url(r'^reset/password_reset', password_reset,
        {'template_name': 'registration/password_reset_form.html',
         'email_template_name': 'registration/password_reset_email.html'},
        name='password_reset'),
    url(r'^password_reset_done', password_reset_done,
        {'template_name': 'registration/password_reset_done.html'},
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,
        {'template_name': 'registration/password_reset_confirm.html'},
        name='password_reset_confirm'
        ),
    url(r'^reset/done', password_reset_complete, {'template_name': 'registration/password_reset_complete.html'},
        name='password_reset_complete'),
]

from django.conf.urls.static import static
from django.conf import settings

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

