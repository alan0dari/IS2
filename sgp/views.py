import datetime

from django.contrib.admin.options import get_content_type_for_model
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.core.mail import EmailMessage

from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.utils.encoding import force_text
from django.views.generic import ListView

from proyectos.models import Proyecto
from sgp.models import LogSistema
from sprint.models import Sprint
from sprint.models import BurndownChart
from team.models import team_member
from .forms import LoginForm
"""
Todas las vistas de la aplicacion sgp 
Actualmente soporta las siguientes 3 vistas:


1. **Conectar** - la vista de login
2. **Logout** - la vista de desconexion
3. **Principal** -vista principal del sistema
3. **Historial** -vista de listado de logs del sistema


"""

#las pruebas unitarias de las vistas se encuentran en [[test.py]]

# === conectar ===
def conectar(request):
    """
    **Parametros:**


    1. request

    **Return: **
    redirecciona a la vista principal
    """
    mensaje=None
    if request.method == "POST":
        formulario = LoginForm(request.POST)
        if formulario.is_valid():
            nombre = request.POST['nombre']
            password = request.POST['password']
            usuario = authenticate(username=nombre, password=password)
            if usuario is not None:
                if usuario.is_active:
                    login(request, usuario)
                    return redirect('principal')
                else:
                    mensaje ="El usuario esta inactivo"
            else:
                    mensaje ="Usuario no existe o credenciales invalidas"
    else:
        formulario=LoginForm()
    return render(request, 'usuarios/login.html', {"mensaje": mensaje, "form": formulario})

# === desconectar ===
@login_required
def desconectar (request):
    """
    **Parametros:**


    1. request

    **Return: **
    redirecciona a la vista de login
            """
    logout(request)
    return redirect('login')

# === principal ===
class PaginaPrincipalView(LoginRequiredMixin, ListView):
    """
                Vista de la página principal
                :param request:
                :return muestra la página de inicio del sistema
                """
    login_url = "/login/"
    template_name = "inicio.html"
    context_object_name = "all_proyectos"

    def get_queryset(self):

        query = BurndownChart.objects.all()
        fecha_actual = datetime.date.today()
        for bc in query:
            if bc.sprint.estado != "Finalizado" and bc.ultima_fecha < fecha_actual:
                bc.contador_dias += (fecha_actual - bc.ultima_fecha).days
                bc.ultima_fecha = fecha_actual
                print("Hola")
                horas_restantes = 0
                listaUS = bc.sprint.sprintBacklog.all()
                for us in listaUS:
                    horas_restantes += us.tiempo_estimado - us.tiempo_dedicado
                bc.horas_restantes = horas_restantes
                bc.abscisas.append(bc.ultima_fecha)
                bc.ordenadas.append(horas_restantes)
                bc.save()

        q=""
        if "q" in self.request.GET:
            q=self.request.GET["q"]
        lista=Proyecto.objects.all()
        if self.request.user.has_perm("proyectos.view_proyecto") :
            for p in Proyecto.objects.all():
                if p.nombre.find(q) == -1:
                    lista=lista.exclude(id=p.id)
            return lista
        for p in Proyecto.objects.all():
            if not self.request.user.has_perm("proyectos.view_proyecto",p) :
                lista=lista.exclude(id=p.id)
            if p.nombre.find(q) == -1:
                lista=lista.exclude(id=p.id)
        query = Sprint.objects.filter(notificadoFin='No',estado='En ejecución')
        for s in query:
            fechaFin=s.fechaFinalizacion
            print(fechaFin)
            fechaActual=datetime.date.today()
            print(fechaActual)
            if fechaActual >= fechaFin:
                proyecto=s.proyecto
                rol=Group.objects.get(name='Scrum Master')
                print(team_member.objects.filter(proyecto=proyecto,rol=rol))
                scrum_master = team_member.objects.filter(proyecto=proyecto,rol=rol)
                if scrum_master.count() > 0:
                    for sm in scrum_master:
                        try:
                            email = sm.usuario.email
                            html_content = "Buenas %s <br>La fecha de finalizacion prevista( %s ) para el Sprint en ejecucion %s en el proyecto %s ha sido sobrepasada<br>"
                            message = EmailMessage(subject="Notificacion:Fecha de Finalizacion de Sprint prevista sobrepasada", body=html_content % (
                            sm.usuario.username,s.fechaFinalizacion,s, s.proyecto.nombre ), to=[email])
                            message.content_subtype = 'html'
                            message.send()
                            s.notificadoFin = "Si"
                            s.save()
                        except:
                            print("No se envió el correo de notificación ")




        return lista


# === Historial ===
class Historial(ListView):
    model=LogSistema
    template_name = 'historial.html'
    paginate_by = 5


# === historial2 ===
def historial2 (request,form,flag,mensaje):
    LogSistema.objects.create(
        user_id=request.user.pk,
        content_type_id=get_content_type_for_model(form.instance).pk,
        object_id=form.instance.id,
        object_repr=force_text(form.instance),
        action_flag=flag,
        change_message=mensaje,
    )
# === historial ===
def historial (self,flag,mensaje):
    LogSistema.objects.create(
        user_id=self.request.user.pk,
        content_type_id=get_content_type_for_model(self.object).pk,
        object_id=self.object.id,
        object_repr=force_text(self.object),
        action_flag=flag,
        change_message=mensaje,
    )


