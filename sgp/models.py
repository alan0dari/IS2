from django.contrib.admin.models import LogEntry
from django.db import models


class LogSistema(LogEntry):
    class Meta:
        """
            Meta-clase de LogSistema:

        """
        permissions = (
            ('view_logsistema', 'Ver log del sistema'),
        )

    sistema=models.BooleanField(default=True)


