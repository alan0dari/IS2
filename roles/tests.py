from django.contrib.auth.models import User, Group
from django.test import TestCase

"""
Todas las pruebas de la aplicacion roles 
actualmente soporta las siguientes 5 pruebas:


1. **test_registrar_rol_valido** 
2. **test_registrar_rol_invalido** 
3. **test_modificar_rol_valido**
4. **test_modificar_rol_invalido**
5. **test_eliminar_rol_valido**
6. **test_eliminar_rol_invalido**
7. **test_listar_rol**
8. **test_detalle_rol_valido**
9. **test_detalle_rol_invalido**

**Observaciones:**

1. Correr cada clase test de forma independiente
"""


class test_roles(TestCase):
    def setUp(self):
        User.objects.create_superuser(username='Nati', password='adminadmin', email='')

    def test_registrar_rol_valido(self):
        # === test_registrar_rol_valido ===
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Developer', 'permissions': {'1': 1, '2': 2}})

        rol = Group.objects.get(name='Developer')
        print('El rol creado es: ', rol)

        rol = Group.objects.get(name='Developer')
        print('Listado de permisos:')
        for x in rol.permissions.all():
            print(x.name)

        var = Group.objects.all()
        print('Si el registro de rol fue válido, que es lo que se espera, se retornará 1')
        print(var.count())
        self.assertEquals(var.count(), 1)

    def test_registrar_rol_invalido(self):
        # === test_registrar_rol_invalido ===
        print('Inicio de test registrar_rol_invalido ')
        self.client.login(username='Nati', password='adminadmin')

        self.client.post('/roles/crear_rol', data={'name': '', 'permissions': {'1': '1', '2': '2'}})
        var = Group.objects.all()
        print('Si el registro de rol fue inválido, que es lo que se espera, se retornará 0')
        print(var.count())
        self.assertEquals(var.count(), 0)

    def test_modificar_rol_valido(self):
        print('Inicio de test modificar_rol_valido ')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        rol = Group.objects.all().get()

        print('Rol antes de la modificación:\nId del rol creado:', rol.id, '\nNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)

        rol = Group.objects.all().get()
        url = "/roles/modif_rol/" + str(rol.id) + "/"
        self.client.post(url, data={'name': 'Tester', 'permissions': {'1': 1, '2': 2}})

        print('Rol despues de la modificación:\nId del rol creado:', rol.id, '\nNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)

        self.assertEquals(rol.name, 'Tester')
        print('El rol se modificó correctamente')

    def test_modificar_rol_invalido(self):
        print('Inicio de test modificar_rol_invalido')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        rol = Group.objects.all().get()
        print('Rol antes de la modificación:\nId del rol creado:', rol.id, '\nNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)

        self.client.post('/roles/modif_rol/1/', data={'name': '', 'permissions': {'1': 1, '2': 2}})

        rol = Group.objects.all().get()
        print('Rol despues de la modificación:\nId del rol creado:', rol.id, '\nNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)

        self.assertEquals(rol.name, 'Tester')
        print('El rol no se modificò como se esperaba')

    def test_eliminar_rol_valido(self):
        print('Inicio de eliminar_rol_valido')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        print('Roles antes de la eliminacion: ', Group.objects.all())
        rol = Group.objects.all().get()
        url = "/roles/eliminar_rol/" + str(rol.id) + "/"
        response = self.client.post(url)
        print('Despues de eliminar el rol: ', Group.objects.all())

        self.assertEquals(response.status_code, 302)
        print('El rol se eliminò correctamente :)')

    def test_eliminar_rol_invalido(self):
        print('Inicio de eliminar_rol_invalido')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        print('Roles antes de la eliminacion: ', Group.objects.all())

        response = self.client.post('/roles/eliminar_rol/18/')
        print('Despues de eliminar el rol: ', Group.objects.all())

        self.assertEquals(response.status_code, 404)
        print('El rol no se eliminò')

    def test_listar_rol(self):
        print('Inicio de listar_rol_valido')
        self.client.login(username='Nati', password='adminadmin')
        self.test_registrar_rol_valido()

        response = self.client.get('/roles/listar_rol')
        self.assertEqual(response.status_code, 200)
        print('Se muestra la lista de roles correctamente')

    def test_detalle_rol_valido(self):
        print('Inicio de detalle_rol_valido')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        rol = Group.objects.all().get()
        url = "/roles/detalle_rol/" + str(rol.id) + "/"
        response = self.client.get(url)
        print('Visualizar detalles \n \tNombre: ', rol.name, '\n\tPermisos: ')
        for x in rol.permissions.all():
            print(x.name)

        self.assertEquals(response.status_code, 200)
        print('Detalles mostrados correctamente')

    def test_detalle_rol_invalido(self):
        print('Inicio de detalle_rol_invalido')
        self.client.login(username='Nati', password='adminadmin')
        self.client.post('/roles/crear_rol', data={'name': 'Tester', 'permissions': {'3': 3, '4': 4, '5': 5}})

        response = self.client.get('/roles/detalle_rol/18/')

        self.assertEquals(response.status_code, 404)
        print('Como se espera, los detalles no pueden ser mostrados porque el rol no existe')