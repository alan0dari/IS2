from django.contrib.admin.models import ADDITION, CHANGE, DELETION
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group,Permission
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, DeleteView, DetailView

from proyectos.models import Proyecto
from roles.forms import GroupForm
from sgp.views import historial2, historial
from team.models import team_member

"""
La aplicacion roles actualmente 
soporta las siguientes 5 vistas:


1. **listar_rol** - Vista para la lista de roles
2. **crear_rol_view** - Vista para la creación de un rol
3. **modificar_rol** - Vista para la modificación de un rol
4. **eliminar_rol** - Vista para la eliminación de un rol
5. **detalle_rol** - Vista para visualizar detalles de un rol

"""
# Las pruebas unitarias de las vistas se encuentran en [[tests.py]]

# === crear_rol ===
@login_required
@permission_required('auth.add_group')
def crear_rol_view(request):
    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            historial2(request,form,ADDITION,'Nuevo Rol')
            return redirect('roles:listar')
        else:
            return redirect(reverse_lazy('roles:listar') + "?err_name=True")
    else:
        form = GroupForm()
        tipo_proyecto = ContentType.objects.get_for_model(Proyecto)
        lista = Permission.objects.filter(content_type=tipo_proyecto).exclude(codename="add_proyecto")

    return render(request,'roles/roles_form.html', {'form': form,
                                                    'lista_permisos': lista})


# === listar_roles ===
class listar_rol(PermissionRequiredMixin,ListView):
    """
    **Ancestros**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. ListView: Renderiza alguna lista de objetos.

    **Retorna:** url a listar_rol
    """
    model = Group
    template_name = "roles/rol_listar.html"
    paginate_by = 8
    permission_required = 'auth.change_group'

    def get_context_data(self, *, object_list=None, **kwargs):
        contexto = super().get_context_data()
        if "err_name" in self.request.GET:
            contexto["nombre_no_disponible"]= self.request.GET["err_name"] == "True"
        return contexto



# === modificar_rol ===
class modificar_rol(PermissionRequiredMixin,UpdateView):
    """

    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. UpdateView: Vista para modificar un objeto,con una respuesta renderizada por un template

    **Retorna**: url a listar_rol
    """

    model = Group
    form_class = GroupForm
    permission_required = 'auth.change_group'
    template_name = 'roles/roles_form.html'

    def get_success_url(self):
        # `Override:` Dado un exito en la operación, actualiza los permisos de los integrantes (en sus
        # equipos) que poseen el rol modificado.
        for integrante in team_member.objects.filter(rol=self.object):
            integrante.save()
        historial(self,CHANGE,'Modificacion de Rol')
        return reverse_lazy('roles:listar')

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        tipo_proyecto=ContentType.objects.get_for_model(Proyecto)
        if self.object.name == "Administrador" or self.object.name == "Usuario Normal":
            add_proy = Permission.objects.filter(codename="add_proyecto")
            contexto["lista_permisos"] = Permission.objects.exclude(content_type=tipo_proyecto)
            contexto["lista_permisos"] = contexto["lista_permisos"].union(add_proy)
        else:
            contexto["lista_permisos"] = Permission.objects.filter(content_type=tipo_proyecto)
            contexto["lista_permisos"] = contexto["lista_permisos"].exclude(codename="add_proyecto")

        return contexto

    def form_invalid(self, form):
        return redirect(reverse_lazy('roles:listar') + "?err_name=True")

# === eliminar_rol ===
class eliminar_rol(PermissionRequiredMixin,DeleteView):
    """

    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. DeleteView: Vista para borrar un objeto recolectado con  `self.get_object()`, con una respuesta renderizada con una template

    **Retorna:** url a listar_rol
    """

    model = Group
    permission_required = 'auth.delete_group'
    template_name = 'roles/rol_eliminar.html'
    def get_success_url(self):
        # `Override:` Dado un exito en la operación, desacredita los permisos al los integrantes afectados
        for integrante in team_member.objects.filter(rol=self.object):
            integrante.save()
        historial(self,DELETION,'Borrado de Rol')
        return reverse_lazy('roles:listar')


# === detallo_rol ===
class detalle_rol(DetailView):
    """
    **Ancestros:**

    1.DetailView: por defecto esta es una instancia
        de modelo para `self.queryset` pero
        la vista soporta mostrar cualquier
        objeto si se sobreescribe `self.get_object()

    **Retorna:** Renderiza la vista detalle del objeto
    """

    template_name = "roles/rol_detalle.html"
