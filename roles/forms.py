from django import forms
from django.contrib.auth.models import Group

"""
Form de la aplicación roles:

1. **GroupForm** - usado para la edicion y creación de roles


"""


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = [
            'name',
            'permissions',
        ]
        labels = {
            'name':'Nombre del rol',
            'permissions':'Permisos',
        }
        widgets = {
            'permissions': forms.SelectMultiple(attrs={'style': 'width: 450px; height: 350px;'})
        }
