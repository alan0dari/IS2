from django.conf.urls import url
from django.contrib.auth.models import Group

from roles.views import listar_rol, crear_rol_view, modificar_rol, eliminar_rol, detalle_rol

urlpatterns = [
    url(r'^listar_rol$', listar_rol.as_view(model=Group,), name='listar'),
    url(r'^crear_rol$', crear_rol_view, name='crear'),
    url(r'^modif_rol/(?P<pk>\d+)/$', modificar_rol.as_view(model=Group,), name='modificar'),
    url(r'^eliminar_rol/(?P<pk>\d+)/$', eliminar_rol.as_view(model=Group,), name='eliminar'),
    url(r'^detalle_rol/(?P<pk>\d+)/$', detalle_rol.as_view(model=Group,), name='ver'),
]


