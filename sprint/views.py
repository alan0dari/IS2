import datetime
from matplotlib import pyplot as plt
from matplotlib import dates as mdates

from django.contrib.admin.models import ADDITION, DELETION, CHANGE
from django.contrib.admin.models import DELETION
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.core.mail import EmailMessage
import datetime
from django.contrib.auth.models import Group, User
from guardian.mixins import PermissionRequiredMixin

from proyectos.models import Proyecto
from proyectos.views import log
from proyectos.views import log
from sgp.utils import render_to_pdf
from sprint.models import Sprint,reuniones
from team.models import team_member
from team.views import has_perm

from sprint.models import Sprint
from tipous.models import Fase
from userstory.models import UserStory
from sprint.models import BurndownChart

#=== crear ====
def crear(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la creacion de un Sprint, o un
    mensaje de alerta si no los posee.
    """
    #`Return:` HttpResponse de acuerdo al caso.
    if has_perm(request.user,'proyectos.add_Sprint',kwargs['pk']):
        return createSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== importar ===
def importar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la importacion de Sprints, o un
    mensaje de alerta si no los posee.
    """
    #`Return:` HttpResponse de acuerdo al caso.
    perms = has_perm(request.user,'proyectos.add_Sprint',kwargs['pk'])
    perms&= has_perm(request.user,'proyectos.crear_UserStory',kwargs['pk'])
    if perms:
        return ImportSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== listar ===
def listar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con un listado de los Sprints del proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    if has_perm(request.user,'proyectos.view_Sprint',kwargs['pk']):
        return listSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== editar ===
def editar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la edicion de los atributos del sprint , o
    un mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyectos:sprint:editar", *args, **kwargs)
    if has_perm(request.user,'proyectos.edit_Sprint',kwargs['pk']):
        return editSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== borrar ===
def borrar(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un pedido de confirmacion para la eliminacion del Sprint, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyectos:sprint:borrar", *args, **kwargs)
    if has_perm(request.user,'proyectos.delete_Sprint',kwargs['pk']):
        return borrarSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== ver ===
def ver(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega toda la informacion referente al Sprint (incluye los resumenes
    de reuniones), o un mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyectos:sprint:ver", *args, **kwargs)
    if has_perm(request.user, 'proyectos.view_Sprint', kwargs['pk']):
        return SprintDetail.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


#=== createSprint
class createSprint(CreateView):
    """
    Menu de adicion de Sprint al proyecto.
    """
    #`Extends:` CreateView; para la visualizacion del menu de adicion de miembros al Equipo.
    model = Sprint
    fields = ['sprintBacklog','duracion','sprintTeam']
    template_name = "sprint/add.html"

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega al nuevo Sprint el indicador del proyecto
        al cual pertenece.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.proyecto=Proyecto.objects.get(id=kwargs['pk'])
        if form.is_valid():
            ret = self.form_valid(form)
            if verificar_asignacion(request, form.instance):
                asignar_US(request, form.instance)
            return ret
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de User Stories disponibles,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(CreateView,self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        US_list = UserStory.objects.filter(proyecto=context['proyecto'])
        US_list = US_list.exclude(estado=3)
        sprints_activos = Sprint.objects.filter(proyecto=context['proyecto'])
        sprints_activos = sprints_activos.exclude(estado="Finalizado")

        for i in sprints_activos:
            for j in i.sprintBacklog.all():
                US_list = US_list.exclude(id=j.id)
        context['US_list'] = US_list
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,ADDITION,'Nuevo Sprint')
        return reverse_lazy("proyectos:sprint:list",kwargs = {'pk': l})

#=== ImportSprint ===
class ImportSprint(ListView):
    """
    Menu de listado de los Sprint del proyecto.
    """
    # `Extends:` ListView; para la visualizacion del menu.
    model = Sprint
    template_name = 'sprint/importar.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de Sprints a fin de proyectar solo los
        importables al proyecto.
        """
        list_proyectos = Proyecto.objects.all().exclude(id=self.kwargs['pk'])
        list_Sprint = Sprint.objects.all()
        aux  = list_Sprint
        for s in aux:
            if str(s.proyecto_id) == self.kwargs['pk']:
                list_Sprint = list_Sprint.exclude(id=s.id)

        for p in list_proyectos:
            view_proy= has_perm(self.request.user,"view_proyecto",p.id)
            view_us= has_perm(self.request.user,"ver_UserStory",p.id)
            view_sprint= has_perm(self.request.user,"view_Sprint",p.id)
            if not (view_proy and view_sprint and view_us):
                aux=list_Sprint
                for s in aux:
                    if s.proyecto_id == p.id:
                        list_Sprint = list_Sprint.exclude(id=s.id)

        return list_Sprint

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        for i in ['add','edit','delete']:
            context['perm_'+i]=has_perm(self.request.user,"proyectos."+i+"_Sprint",self.kwargs["pk"])
        context['perm_import'] = context['perm_add']
        context['perm_import'] &= has_perm(self.request.user,"proyectos.crear_UserStory",self.kwargs["pk"])
        return context

    def post(self, request, *args, **kwargs):
        """
        Define las acciones a realizar para la importacion
        """
        importacion(self.kwargs["pk"],self.kwargs["s"])
        return self.get_success_url()

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        args = self.args
        kwargs = {}
        kwargs['pk'] = self.kwargs['pk']
        return redirect("proyectos:sprint:list", *args, **kwargs)

#=== listSprint ===
class listSprint(ListView):
    """
    Menu de listado miembros del Equipo.
    """
    # `Extends:` ListView; para la visualizacion del menu.
    model = Sprint
    template_name = 'sprint/listar.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de Sprints a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return Sprint.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        for i in ['add','edit','delete']:
            context['perm_'+i]=has_perm(self.request.user,"proyectos."+i+"_Sprint",self.kwargs["pk"])
        context['perm_import'] = context['perm_add']
        context['perm_import'] &= has_perm(self.request.user,"proyectos.crear_UserStory",self.kwargs["pk"])
        return context

#=== borrarSprint===
class borrarSprint (DeleteView):
    """
    Solicitud de confirmacion para borrar el Sprint.
    """
    # `Extends:` DeleteView; para visualizacion de la confirmacion del borrado.
    model = Sprint
    template_name = 'sprint/borrar.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del sprint en cuestion.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['sp'])
        obj = queryset.get()
        if obj.estado == "En ejecución":
            obj = None
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,DELETION,'Borrado de Sprint')
        return reverse_lazy("proyectos:sprint:list",kwargs = {'pk': l})

#=== editSprint ==
class editSprint(UpdateView):
    """
    Menu de edicion de caracteristicas de un Sprint.
    """
    # `Extends:` EditView; para la visualizacion del menu de edicion.
    model = Sprint
    fields = ['sprintBacklog','duracion','sprintTeam']
    template_name = 'sprint/add_editar.html'

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del Sprint a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['sp'], estado="En espera")
        obj = queryset.get()
        return obj

    def post(self, request, *args, **kwargs):
        """
        `Override:` Registra las asiganciones de los US del Sprint.
        """
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            ret = self.form_valid(form)
            if verificar_asignacion(request,form.instance):
                asignar_US(request,form.instance)
            return ret
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de datos para el menu.
        """
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        US_list = UserStory.objects.filter(proyecto=context['proyecto'])
        US_list = US_list.exclude(estado=3)
        sprints_activos = Sprint.objects.filter(proyecto=context['proyecto'])
        sprints_activos = sprints_activos.exclude(estado="Finalizado")
        sprints_activos = sprints_activos.exclude(id=self.kwargs["sp"])
        for i in sprints_activos:
            for j in i.sprintBacklog.all():
                US_list = US_list.exclude(id=j.id)
        context['US_list'] = US_list
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        log(self,CHANGE,'Edicion de Sprint')
        return reverse_lazy("proyectos:sprint:list",kwargs = {'pk': l})

#=== SprintDetail====
class SprintDetail(DetailView):
    """
        Visualizador de detalles del Sprint.
        """
    # `Extends:` DetailView; para la visualizacion de los detalles.
    model = Sprint
    template_name = "sprint/detalle.html"

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del Sprint a mostrar
        """
        if queryset is None:
            queryset = self.get_queryset()
        obj = Sprint.objects.get(id=self.kwargs['sp'])
        return obj
    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(DetailView,self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        context['sprintTeam'] = self.object.sprintTeam.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        horas_asignadas = {}

        for us in context['sprint_backlog']:
            if not us.team_member.id in horas_asignadas:
                horas_asignadas[us.team_member.id] = 0

            horas_asignadas[us.team_member.id] = horas_asignadas[us.team_member.id] + us.tiempo_estimado

        context['horas_asignadas'] = horas_asignadas


        context["perm_control"] = has_perm(self.request.user, "proyectos.ver_control", self.kwargs["pk"])
        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        context['can_execute'] = Sprint.objects.filter(proyecto_id=self.kwargs["pk"],
                                                       estado="En ejecución").count() == 0
        context['can_execute_2'] = UserStory.objects.filter(proyecto_id=self.kwargs["pk"],
                                   estado=2).count() == 0
        context['can_execute_3'] = Proyecto.objects.filter(Q(id=self.kwargs['pk']), Q(estado="En espera de inicio")| Q(estado='Finalizado') | Q(estado='Proyecto Cancelado')).count() == 0
        print( Proyecto.objects.filter(Q(id=self.kwargs['pk']), Q(estado="En espera de inicio")| Q(estado='Finalizado') | Q(estado='Proyecto Cancelado')).count() == 0)
        return context

#=== Cambios de estado ===

#=== pasar_estado ===
def pasar_estado(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    registra el siguiente estado del sprint , o un mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
    if has_perm(request.user,'proyectos.edit_Sprint',kwargs['pk']):
        return ContinuarSprint.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== ContinuarSprint ===
class ContinuarSprint(UpdateView):
    model = Sprint
    fields = ['estado']

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del Sprint a editar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['sp'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        if Sprint.objects.get(id=self.kwargs['sp']).estado=='En ejecución':
            listaUS = Sprint.objects.get(id=self.kwargs['sp']).sprintBacklog.all()
            for us in listaUS:
                if us.estado == 1:
                    us.estado = 0
                    us.save()
        pr=self.kwargs['pk']
        sp=self.kwargs['sp']
        log(self,CHANGE,'Cambia de estado el sprint')

        sprint = Sprint.objects.get(id=self.kwargs['sp'])
        if sprint.estado == "En ejecución":
            print("Entro en el cambio de estado")
            horas_restantes = 0
            lista_us = Sprint.objects.get(id=self.kwargs['sp']).sprintBacklog.all()
            for userstory in lista_us:
                horas_restantes += userstory.tiempo_estimado - userstory.tiempo_dedicado
            bc = BurndownChart.objects.create(sprint=sprint, horas_restantes=horas_restantes, abscisas=[datetime.date.today()], ordenadas=[horas_restantes])
            bc.save()
        if sprint.estado=="Finalizado":
            lista_us = Sprint.objects.get(id=self.kwargs['sp']).sprintBacklog.all()
            for us in lista_us:
                if us.estado == 0:
                    us.prioridad_final=10
                    us.save()
            sprint.fechaFinalizacion=datetime.date.today()
            sprint.save()

        return reverse_lazy("proyectos:sprint:ver",kwargs = {'pk': pr,'sp' : sp})

    def post(self, request, *args, **kwargs):
        # `Override:` Actualmente, ejecuta las mismas acciones que el "padre".
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


    def form_valid(self, form):
        # `Override:` Ejecuta unas verificaciones para mantener la estabilidad
        # de los datos.
        cambios = {
            "En espera": ["En ejecución"],
            "En ejecución": ["Finalizado"],
            "Finalizado": [],
        }
        old = Sprint.objects.get(id=self.object.id).estado
        new = form.instance.estado
        if not (old in cambios and new in cambios[old]):
            form.instance.estado = old
        if form.instance.estado == "En ejecución":
            cant_sprint_ejecucion = Sprint.objects.filter(proyecto=form.instance.proyecto,
                                    estado=new).exclude(id=self.object.id).count()
            cant_us_control = UserStory.objects.filter(proyecto=form.instance.proyecto,
                              estado=2).count()
            if cant_sprint_ejecucion>0 or cant_us_control>0:
                form.instance.estado = "En espera"
            else:
                form.instance.fechaInicio = datetime.date.today()
        return super().form_valid(form)

    def form_invalid(self, form):
        # `Override:` Personaliza la url de redireccion en caso de fracaso en
        # en la operacion.
        args = self.args
        kwargs = {}
        kwargs['pk'] = self.kwargs['pk']
        kwargs['sp'] = self.kwargs['sp']
        return redirect("proyectos:sprint:ver", *args, **kwargs)


#=== View para resumen ===

#=== crear_resumen ===
def crear_resumen(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    recepciona un nuevo registro de reunion.
    """
    #`Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
    if has_perm(request.user,'proyectos.edit_Sprint',kwargs['pk']):
        return addReunion.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== editar_resumen ===
def editar_resumen(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu para la edicion de las reuniones retrospectivas,
    o un mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
    if has_perm(request.user,'proyectos.edit_Sprint',kwargs['pk']):
        return editReunion.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== borrar_resumen ===
def borrar_resumen(request,*args,**kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un pedido de confirmacion para proceder a borrar el item
    """
    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['sp']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
    if has_perm(request.user,'proyectos.edit_Sprint',kwargs['pk']):
        return borrarReunion.as_view()(request,*args,**kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

#=== addReunion ===
class addReunion(CreateView):
    """
    Recepcion de registro de reuniones.
    """
    #`Extends:` CreateView; para las operaciones de alta.
    model = reuniones
    fields = ['titulo','fecha','resumen']

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega a la nueva reunion el indicador implicito del sprint al
        que pertenece.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.sprint=Sprint.objects.get(id=kwargs['sp'])
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr=self.kwargs['pk']
        sp=self.kwargs['sp']
        log(self, ADDITION, 'Nueva registro de reunion')
        return reverse_lazy("proyectos:sprint:ver",kwargs = {'pk': pr,'sp' : sp})

#=== editReunion ===
class editReunion(UpdateView):
    """
    Recepcion de la edicion de la reunion.
    """
    # `Extends:` EditView; para las operaciones de modificacion.
    model = reuniones
    fields = ['titulo','fecha','resumen']

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del resumen a editar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['r'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr=self.kwargs['pk']
        sp=self.kwargs['sp']
        log(self, CHANGE, 'Edicion de  registro de una reunion')
        return reverse_lazy("proyectos:sprint:ver",kwargs = {'pk': pr,'sp' : sp})

#=== borrarReunion ===
class borrarReunion(DeleteView):
    """
    Recepciona la solicitud de borrado de una reunion.
    """
    # `Extends:` DeleteView; para la operación de baja.
    model = reuniones

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del resumen a borrar.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['r'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        sp = self.kwargs['sp']
        log(self,DELETION,'Borrado de registro de una reunion')
        return reverse_lazy("proyectos:sprint:ver", kwargs={'pk': pr, 'sp': sp})

#=== importacion ===
def importacion(proy_destino_id,sprint_id):
    # Funcion auxiliar para importar el sprint con id `sprint_id` tenga el permiso
    # en el proyecto de id `proy_destino_id`.
    sprint_to_import = Sprint.objects.get(id=sprint_id)
    sprint_activos = Sprint.objects.filter(proyecto_id=proy_destino_id,estado="En ejecución").count()
    s = Sprint()

    if sprint_activos > 0 and sprint_to_import.estado == "En ejecución":
        s.estado = "En espera"
    else:
        s.estado = 'En espera'

    s.proyecto_id = proy_destino_id
    s.save()
    for us in sprint_to_import.sprintBacklog.all():
        us.id=None
        us.proyecto_id = proy_destino_id
        us.team_member= None
        us.tipous = None
        us.estado = 1
        us.tiempo_dedicado = 0
        us.save()
        s.sprintBacklog.add(us)
    s.save()
    for reunion in reuniones.objects.filter(sprint_id=sprint_to_import):
        reunion.id = None
        reunion.sprint_id = s.id
        reunion.save()
#=== verificar_asignacion ==
def verificar_asignacion(request,object):
    # Funcion auxiliar para verificar validez de las asignaciones de US en el sprint,
    # extrayendo la información del `request` y complementando con el objeto resultante
    # `object`.
    for i in object.sprintBacklog.all():
        key = "asignar_"+str(i.id)
        print(key)
        if not key in request.POST:
            return False
        if object.sprintTeam.filter(id=request.POST[key]).count()==0:
            return False

    return True
#=== asignar_US ==
def asignar_US(request,object):
    # Funcion auxiliar para realizar las asignaciones de US en el sprint, extrayendo
    # la información del `request` y complementando con el objeto resultante `object`.
    try:
        for us in object.sprintBacklog.all():
            key = "asignar_" + str(us.id)
            team = object.sprintTeam.get(id=request.POST[key])
            us.team_member = team
            us.save()
            print(str(us.nombre) + " = "+ str(team.usuario))

            query = User.objects.filter(username=team.usuario)
            if query.count() > 0:
                email = team.usuario.email
                print('email:', email)
                html_content = "Le ha sido asignado el User Story: %s en el proyecto: %s<br> Descripción del US: %s <br>Prioridad: %s<br>"
                message = EmailMessage(subject='Asignación de User Story',
                                       body=html_content % (us.nombre, us.proyecto.nombre, us.descripcion_corta, us.prioridad), to=[email])
                message.content_subtype = 'html'
                message.send()
                print("Notificación enviada a", team.usuario)
            else:
                print("Fallo en la notificación via email")
    except :
        raise Exception("Fallo en la Asignacion")


#=== GeneratePDF ==
class GeneratePDF(View):
    """
     Genera un pdf con los US asignados del Team member en un Sprint.
     """
    model=Sprint
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteEnProyecto.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteEnSprint.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        lista_us=Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all().filter(team_member_id=self.kwargs['tid'])
        context = {
            "Nombre": team_member.objects.get(proyecto=self.proyecto, id=self.kwargs['tid']),
            "hora": datetime.datetime.now(),
            "proyecto":Proyecto.objects.get(id=self.kwargs['pk']),
            "object_list":lista_us,
            "Sprint":Sprint.objects.get(pk=self.kwargs['sp']),
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteEnSprint.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(View, self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        return context



#=== GeneratePDFSprintBacklog ==
class GeneratePDFSprintBacklog(View):
    """
         Genera un pdf condel Sprint backlog de un Sprint.
    """
    model=Sprint
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteEnProyecto.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteEnSprint.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        lista_us=Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all()
        context = {
            "hora": datetime.datetime.now(),
            "proyecto":Proyecto.objects.get(id=self.kwargs['pk']),
            "sprintbacklog":lista_us,
            "Sprint":Sprint.objects.get(pk=self.kwargs['sp']),
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteEnSprint.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(View,self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        return context
#=== GeneratePDFusprioridad ==
class GeneratePDFusprioridad(View):
    """
         Genera un pdf con los us priorizados  de un Sprint.
    """
    model=Sprint
    permission_required = 'proyectos.ver_UserStory'
    raise_exception = True
    template_name = 'pdf/reporteEnusprioridad.html'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get(self, request, *args, **kwargs):
        """
        `Override:` Agrega el contexto y genera el pdf.
        """
        template = get_template('pdf/reporteEnusprioridad.html')
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])

        lista_us=Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.filter(Q(prioridad_final=10),Q(estado=0) | Q(estado=2))
        context = {
            "hora": datetime.datetime.now(),
            "proyecto":Proyecto.objects.get(id=self.kwargs['pk']),
            "sprintbacklog":lista_us,
            "Sprint":Sprint.objects.get(pk=self.kwargs['sp']),
        }
        html = template.render(context)
        pdf = render_to_pdf('pdf/reporteEnusprioridad.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "reporte_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(View,self).get_context_data(**kwargs)
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(
                    team_member_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(team_member_id=self.request.GET['filter_by'])
        else:
            print("puro")

        context["perm_us_view"] = has_perm(self.request.user,"proyectos.ver_UserStory",self.kwargs["pk"])
        context["perm_us_edit"] = has_perm(self.request.user,"proyectos.modificar_UserStory",self.kwargs["pk"])
        context["perm_sprint_edit"] = has_perm(self.request.user,"proyectos.edit_Sprint",self.kwargs["pk"])
        return context
#=== Control ==
class Control(ListView):
    template_name = 'sprint/control_calidad.html'
    def get_queryset(self):
        """
        `Override:` personaliza la lista de US a fin de proyectar solo los que cuentan con estado Control de Calidad.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all().filter(estado=2)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        context['sprint'] = Sprint.objects.get(id=self.kwargs['sp'])
        context['lista'] = Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all().filter(estado=2)
        return context

#=== ControlScrum ==
class ControlScrum(ListView):
    """
    Listado de US del sprint.
    """
    template_name = 'sprint/control_scrum.html'
    def get_queryset(self):
        """
        `Override:` personaliza la lista del sprint backlog.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all()

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        context['sprint'] = Sprint.objects.get(id=self.kwargs['sp'])
        context['lista'] = Sprint.objects.get(pk=self.kwargs['sp']).sprintBacklog.all()
        return context



#=== ControlEditar ==
class ControlEditar(PermissionRequiredMixin,UpdateView):
    """
    Menu de edicion de campos de US dentro del control de calidad.
    """
    # `Extends:` EditView; para la visualizacion del menu de edicion.
    model = UserStory
    template_name = 'sprint/control_editar.html'
    fields = ['estado','subfase','fase_actual']
    permission_required = 'proyectos.ver_control'

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def post(self, request, *args, **kwargs):
        us = UserStory.objects.get(id=self.kwargs["usid"])
        us.historialus_set.create(nombre=us.nombre, descripcion_corta=us.descripcion_corta,
                                  descripcion_larga=us.descripcion_larga, prioridad=us.prioridad,
                                  prioridad_final=us.prioridad_final, tiempo_estimado=us.tiempo_estimado,
                                  tipo_us=us.tipo_us, valor_negocio=us.valor_negocio,
                                  valor_tecnico=us.valor_tecnico, fecha_modificacion=us.fecha_modificacion,
                                  tiempo_dedicado=us.tiempo_dedicado
                                  , fase_actual=us.fase_actual, subfase=us.subfase, estado=us.estado)
        context = super(UpdateView, self).post(self, request, *args, **kwargs)
        return context

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del US.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['usid'])
        obj = queryset.get()
        return obj
    def get_context_data(self, **kwargs):
        """
        `0verride:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(UpdateView,self).get_context_data(**kwargs)
        context['proyecto']=Proyecto.objects.get(id=self.kwargs['pk'])
        context['sprint'] = Sprint.objects.get(id=self.kwargs['sp'])
        context['fase_list'] =Fase.objects.filter(tipous=self.get_object().tipo_us.id)
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l=self.kwargs['pk']
        sp = self.kwargs['sp']
        log(self,CHANGE,'US modificado en control de calidad')
        return reverse_lazy("proyectos:sprint:control",kwargs = {'pk': l,'sp': sp})
#=== BurndownChartDetailView ==
class BurndownChartDetailView(DetailView, PermissionRequiredMixin):

    permission_required = "view_Sprint"
    model = BurndownChart
    template_name = "sprint/burndownchart.html"

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        `0verride:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(DetailView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['sprint'] = Sprint.objects.get(id=self.kwargs['sp'])

        inicio_grafico = context['sprint'].fechaInicio
        context['inicio_grafico'] = inicio_grafico.strftime("%d/%m/%Y")

        if context['sprint'].estado == "En ejecución":
            fin_grafico = None
        else:
            fin_grafico = context['sprint'].fechaFinalizacion

        horas_Sprint = 0
        cambios = {}

        for us in context['sprint'].sprintBacklog.all():
            horas_Sprint += us.tiempo_estimado
            version_anterior = None
            for version in us.historialus_set.all():
                comparar_registros(version_anterior,version,
                                   inicio_grafico,fin_grafico,cambios)
                version_anterior = version
            comparar_registros(version_anterior, us,
                               inicio_grafico, fin_grafico,cambios)

        if fin_grafico == None:
            fin_grafico = datetime.date.today()

        fin_estimado = inicio_grafico + datetime.timedelta(context['sprint'].duracion - 1)

        context['eje_x'] = ['Inicio']
        context['eje_y'] = [horas_Sprint]
        context['eje_y_2'] = [horas_Sprint]
        context['eje_y_3'] = [horas_Sprint]

        tasa_caida_linea_azul = horas_Sprint/context['sprint'].duracion
        tasa_caida_linea_naranja = 0

        linea_azul = horas_Sprint - tasa_caida_linea_azul
        linea_naranja= horas_Sprint

        cantidad_dias = 0
        cantidad_dias_previo_al_fin = 0

        hora_trabajadas= 0
        d = inicio_grafico

        while d<=fin_grafico or d<=fin_estimado or linea_naranja>0:

            cantidad_dias= cantidad_dias + 1
            context['eje_x'] += [ d.strftime("%d/%m/%Y") ]

            if d in cambios:
                hora_trabajadas = hora_trabajadas + cambios[d]

            if d<=fin_grafico:
                linea_naranja = horas_Sprint - hora_trabajadas
                context['eje_y'] += [ linea_naranja ]
                context['eje_y_3'] += [ linea_naranja ]
                cantidad_dias_previo_al_fin = cantidad_dias_previo_al_fin + 1
                tasa_caida_linea_naranja = hora_trabajadas/cantidad_dias_previo_al_fin
            elif linea_naranja>0 and round(tasa_caida_linea_naranja,1) > 0:
                linea_naranja = linea_naranja - tasa_caida_linea_naranja
                context['eje_y_3'] += [round(max(linea_naranja,0),2) ]
            else:
                linea_naranja=0

            if d<=fin_estimado:
                context['eje_y_2'] += [ round(linea_azul,2) ]
                linea_azul = linea_azul - tasa_caida_linea_azul

            context['fin_grafico'] = d.strftime("%d/%m/%Y")
            d = d + datetime.timedelta(1)



        return context

#=== comparar_registros ==
def comparar_registros(version_anterior,version_actual,
                       inicio_grafico,fin_grafico,cambios):
    """
        Compara dos versiones consecutivas del user story. Si la modificacion
        se encuentra dentro de los margenes de tiempo del Sprint, añade las horas que
        se trabajo al diccionario "cambios".
    """
    try:
        ultima_modificacion = version_actual.fecha_modificacion.date()
        if ultima_modificacion >= inicio_grafico and (fin_grafico == None or ultima_modificacion <= fin_grafico):
            if version_anterior == None:
                agregar_al_diccionario(cambios, ultima_modificacion,
                                       version_actual.tiempo_dedicado)
            else:
                agregar_al_diccionario(cambios, ultima_modificacion,
                                       version_actual.tiempo_dedicado - version_anterior.tiempo_dedicado)

    except :
        print("Ndoikoi Revision Historial")
#=== agregar_al_diccionario ==
def agregar_al_diccionario(dict,key,value):
    """
        Funcion auxiliar para sumar un valor "`value`", al valor envuelto en el
        diccionario "`dict`" bajo la llave "`key`".
    """
    if not key in dict:
        dict[key] = 0

    dict[key] = dict[key] + value
