import datetime

from django.contrib.postgres.fields import ArrayField
from django.db import models


from sgp.settings import MEDIA_ROOT
from team.models import team_member
from userstory.models import UserStory
from proyectos.models import Proyecto

# === Modelos para sprint ===
class Sprint (models.Model):
    """
        Clase Sprint:
    Soporte para la seleccion de labores a cumplir en un determinado tiempo de ejecución.
    Las labores a contemplar serán determinadas en la clase UserStory
    """
    class Meta:
        """
        **Meta-Clase**:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ["estado","fechaFinalizacion"]
    estado = models.CharField(max_length=18,default="En espera")
    sprintBacklog = models.ManyToManyField(UserStory)
    sprintTeam = models.ManyToManyField(team_member)
    duracion = models.PositiveIntegerField(default=5)
    fechaInicio = models.DateField(null=True)
    fechaFinalizacion = models.DateField(null=True)
    proyecto = models.ForeignKey(Proyecto,on_delete=models.CASCADE)
    notificadoFin = models.CharField(max_length=5,default='No')

    def __str__(self):
        return str(self.id)+str(self.proyecto.nombre)

    def save(self, force_insert=False, force_update=False, using=None,update_fields=None):
        if(self.estado!="En espera" and self.estado!="En ejecución" and self.estado!="Finalizado"):
            print("Estado no valido : "+self.estado)
            self.estado="En espera"
        if (self.fechaInicio!=None and self.fechaFinalizacion==None):
            calendario = datetime.timedelta(max(self.duracion-1,0))
            self.fechaFinalizacion = calendario + self.fechaInicio
        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)

class reuniones (models.Model):
    """
        Clase Reuniones:
    Clase auxiliar que posibilita el registro de pequeñas reuniones retrospectivas dentro del Sprint.
    """
    class Meta:
        """
        **Meta-Clase**:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ["-fecha"]
    fecha = models.DateField()
    titulo = models.CharField(max_length=55)
    resumen = models.CharField(max_length=280)
    sprint = models.ForeignKey(Sprint,on_delete=models.CASCADE)


class BurndownChart(models.Model):
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    horas_restantes = models.IntegerField(default=0)
    contador_dias = models.IntegerField(default=1)
    ultima_fecha = models.DateField(auto_now_add=True)
    abscisas = ArrayField(models.DateField(), blank=True)
    ordenadas = ArrayField(models.IntegerField(blank=True), blank=True)


