from django.conf.urls import url




from .views import crear, editar, borrar, listar, importar, ver, crear_resumen, editar_resumen, borrar_resumen, \
    pasar_estado, GeneratePDF, Control, ControlEditar, GeneratePDFSprintBacklog, ControlScrum, BurndownChartDetailView, \
    GeneratePDFusprioridad

app_name = 'sprint'
urlpatterns = [
    url(r'^$', listar, name='list'),
    url(r'^add$', crear, name='agregar'),
    url(r'^importar$',importar , name='importar'),
    url(r'^importar/(?P<s>\d+)$',importar , name='importar_sprint'),
    url(r'^(?P<sp>\d+)$', ver, name='ver'),
    url(r'^(?P<sp>\d+)/controlscrum$', ControlScrum.as_view(), name='controlscrum'),
    url(r'^(?P<sp>\d+)/control$',Control.as_view(), name='control'),
    url(r'^(?P<sp>\d+)/control/(?P<usid>\d+)$', ControlEditar.as_view(), name='control_editar'),
    url(r'^(?P<sp>\d+)/borrar$', borrar, name='borrar'),
    url(r'^(?P<sp>\d+)/editar$', editar, name='editar'),
    url(r'^(?P<sp>\d+)/continuar$', pasar_estado, name='continuar'),
    url(r'^(?P<sp>\d+)/pdf$', GeneratePDFSprintBacklog.as_view(), name='pdf'),
    url(r'^(?P<sp>\d+)/pdfUS$', GeneratePDFusprioridad.as_view(), name='pdfus'),
    url(r'^(?P<sp>\d+)/pdf/(?P<tid>\d+)$', GeneratePDF.as_view(), name='pdf'),
    url(r'^(?P<sp>\d+)/add/$', crear_resumen, name='agregar_resumen'),
    url(r'^(?P<sp>\d+)/(?P<r>\d+)$', ver, name='ver_resumen'),
    url(r'^(?P<sp>\d+)/borrar/(?P<r>\d+)$', borrar_resumen, name='borrar_resumen'),
    url(r'^(?P<sp>\d+)/editar/(?P<r>\d+)$', editar_resumen, name='editar_resumen'),
    url(r'^(?P<sp>\d+)/burndownchart/$', BurndownChartDetailView.as_view(), name='burndown_chart'),
]