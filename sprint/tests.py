from django.test import TestCase

from django.contrib.auth.models import User, Group
from team.models import team_member
from proyectos.models import Proyecto
from sprint.models import Sprint, reuniones
from team.models import team_member
from tipous.models import TipoUS, Fase
from userstory.models import UserStory
from poblar import poblar


class test_Sprint(TestCase):
    # `Override:` genera los datos de prueba.
    def setUp(self):
        poblar()

    # test de adiciom
    def test_adicion(self):
        # Intenta agregar un sprint al proyecto
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/add"
        datos = {
            "duracion": "3",
            "sprintBacklog": {UserStory.objects.get(nombre="US 4").id},
            "sprintTeam": {team_member.objects.get(proyecto_id=id_proyecto).id}
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        antes = Sprint.objects.filter(proyecto=id_proyecto).count()
        respuesta = self.client.post(url, data=datos)
        despues = Sprint.objects.filter(proyecto=id_proyecto).count()
        self.assertEquals(respuesta.status_code, 302, msg="Error en la creacion")
        self.assertEquals(antes + 1, despues, "No se creo el sprint")

    # test_adicion 2
    def test_adicion_2(self):
        # Intenta agregar un sprint al proyecto, careciendo permisos
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/add"
        datos = {
            "duracion": "3",
            "sprintBacklog": {UserStory.objects.get(nombre="US 4").id},
            "sprintTeam": {team_member.objects.get(proyecto_id=id_proyecto).id}
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Natalia", password="adminadmin")
        antes = Sprint.objects.filter(proyecto=id_proyecto).count()
        respuesta = self.client.post(url, data=datos)
        despues = Sprint.objects.filter(proyecto=id_proyecto).count()
        self.assertNotEquals(respuesta.status_code, 302, msg="No se informo del error")
        self.assertEquals(antes, despues, "Se creo el sprint")

    # Se etest editar
    def test_editar(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_Sprint = Sprint.objects.all().get().id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/" + str(id_Sprint) + "/editar"
        datos = {
            "duracion": "3",
            "sprintBacklog": {UserStory.objects.get(nombre="US 4").id},
            "sprintTeam": {team_member.objects.get(proyecto_id=id_proyecto).id}
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=datos)
        self.assertEquals(respuesta.status_code, 302, msg="No se informo del error")
        print(respuesta)
        self.assertEquals(Sprint.objects.all().get().sprintBacklog.all().count(),
                          1, "No se cambio el Sprint Backlog")

    # test de borrado
    def test_delete(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_Sprint = Sprint.objects.all().get().id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/" + str(id_Sprint) + "/borrar"
        self.client.login(username="Natalia", password="adminadmin")
        respuesta = self.client.post(url)
        Nati_borrar = "Un usuario sin permisos recibio confirmacion de borrado"
        self.assertNotEquals(respuesta.status_code, 302, msg=Nati_borrar)
        self.assertEquals(Sprint.objects.all().count(), 1, msg=Nati_borrar)
        self.client.logout()
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la expulsion")
        self.assertEquals(Sprint.objects.all().count(), 0, msg="No se borro el Sprint")

    # test de reunion
    def test_reunion(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_Sprint = Sprint.objects.all().get().id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/" + str(id_Sprint) + "/"

        delete = reuniones.objects.get(titulo="Reunion 1")
        edit = reuniones.objects.get(titulo="Reunion 2")

        nuevos_datos = {
            "fecha": "2017-02-02",
            "titulo": "Nuevo titulo",
            "resumen": "Nueva descripcion"
        }

        self.client.login(username="Eduardo", password="adminadmin")

        self.client.post(url + "borrar/" + str(delete.id))
        self.assertEquals(reuniones.objects.filter(titulo="Reunion 1").count(),
                          0, "Reunion no borrada")

        self.client.post(url + "editar/" + str(edit.id), nuevos_datos)

        self.assertEquals(reuniones.objects.filter(titulo="Reunion 2").count(),
                          0, "Reunion no editada")

        nuevos_datos["resumen"] = "Reunion 2"
        self.client.post(url + "add/", nuevos_datos)

        self.assertEquals(reuniones.objects.filter(titulo="Reunion 2").count(),
                          0, "Reunion no añadida")

    # test de estado
    def test_estado(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_Sprint = Sprint.objects.all().get().id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/" + str(id_Sprint) + "/continuar"
        datos = {
            "estado": "En ejecución",
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=datos)
        self.assertEquals(respuesta.status_code, 302, msg="Error en el envio")
        print(respuesta)
        self.assertEquals(Sprint.objects.all().get().estado,
                          "En ejecución",
                          "No se cambio el estado")
        datos["estado"] = "En espera"
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=datos)
        self.assertEquals(respuesta.status_code, 302, msg="Error en el envio")
        self.assertNotEquals(Sprint.objects.all().get().estado,
                             "En espera",
                             "Se cambio el estado")

    # test de control
    def test_control(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        us = UserStory.objects.get(nombre="US 4")
        us.estado = 2
        us.tipo_us = TipoUS.objects.get(nombre="Base", proyecto=id_proyecto)
        us.save()
        print(us.estado)

        id_Sprint = Sprint.objects.all().get().id
        Sprint.objects.get(id=id_Sprint).sprintBacklog.add(UserStory.objects.get(nombre="US 4"))
        id_US = UserStory.objects.get(nombre="US 4").id
        url = "/proyecto/" + str(id_proyecto) + "/sprint/" + str(id_Sprint) + "/control/" + str(id_US)
        datos = {
            "estado": 0,
            "fase_actual": Fase.objects.get(tipous=TipoUS.objects.get(nombre="Base", proyecto=id_proyecto),
                                            nombre="Fin").id,
            "subfase": 0,
        }
        print("Post to: " + url)
        print("Data " + str(datos))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=datos)
        print(Sprint.objects.get(id=id_Sprint).sprintBacklog.get(nombre="US 4").estado)
        self.assertEquals(respuesta.status_code, 302, msg="Error en el envio")
        self.assertNotEquals(UserStory.objects.get(nombre="US 4").estado,
                             2,
                             "No se cambio el estado")

    # === test_xreporte sprint ===
    def test_xreporte_sprint(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_Sprint = Sprint.objects.all().get().id
        id_team = team_member.objects.get(usuario=User.objects.get(username='Eduardo').id, proyecto=id_proyecto).id
        print(id_team)
        url = "/proyecto/" + str(id_proyecto) + '/sprint/' + str(id_Sprint) + '/pdf/' + str(id_team)
        respuesta = self.client.get(url)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 200, msg='Se puede visualizar el reporte')
