from django.urls import path,include
from django.conf.urls import url


from .views import ListarTipoUS, RegistrarTipoUS, BorrarTipoUS, EditarTipoUS, RegistrarFase, ListarFases, BorrarFase, \
    EditarFase, TUSImport, ImportarTipoUS

app_name = 'tipous'
urlpatterns = [
    url(r'^$',ListarTipoUS.as_view(), name='listar'),
    url(r'^add$', RegistrarTipoUS.as_view(), name='agregar'),
    url(r'^borrar/(?P<tusid>\d+)/$', BorrarTipoUS.as_view(), name='borrar'),
    url(r'^editar/(?P<tusid>\d+)/$', EditarTipoUS.as_view(), name='editar'),
    url(r'^importar$', ImportarTipoUS.as_view(), name='importar'),
    url(r'^importar/(?P<tusid>\d+)/$', TUSImport.as_view(), name='confirmar_importacion'),

]
"""
    Urls deshabilitadas por Eduardo, 14/05 al inicio de la IT5
    url(r'^(?P<tusid>\d+)/add/$', RegistrarFase.as_view(), name='agregar_fases'),
    url(r'^(?P<tusid>\d+)/$', ListarFases.as_view(), name='listar_fases'),
    url(r'^(?P<tusid>\d+)/borrar/(?P<fid>\d+)$', BorrarFase.as_view(), name='borrar_fase'),
    url(r'^(?P<tusid>\d+)/editar/(?P<fid>\d+)$', EditarFase.as_view(), name='editar_fase'),
"""
