from django import forms

from tipous.models import TipoUS, Fase


class TipousForm(forms.ModelForm):


    class Meta:
        model= TipoUS
        fields = [
           'nombre',
            'descripcion',
        ]

class FaseForm(forms.ModelForm):

    class Meta:
        model=Fase
        fields =[
            'nombre',
            'orden',
        ]
        widgets = {
            'orden': forms.NumberInput(),
        }