from django.contrib.admin.models import ADDITION, DELETION, CHANGE
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, UpdateView, TemplateView
from guardian.mixins import PermissionRequiredMixin
from proyectos.models import Proyecto, Auditoria
from proyectos.views import log
from tipous.forms import TipousForm, FaseForm
from userstory.models import UserStory
from .models import TipoUS, Fase
"""
Todas las vistas de la aplicacion tipous 
Actualmente soporta las siguientes 8 vistas:


1. **RegistroTipoUS** - Se da de alta un tipo de user story
2. **ListarTipoUS** -   Se listan los tipos de user story
3. **BorrarTipoUS** -   Se elimina un tipo de user story
4. **EditarTipoUS** -   Se modifica los tipo de user story
5. **RegistroFase** -   Se da de alta una fase
6. **ListarFase** - Se listan las fases de un tipo de user story(En desuso)
7. **BorrarFase** -   Se borra una fase de un tipo de user story(En desuso)
8. **EditarFase** -   Se modifica una fase de un tipo de user story(En desuso)
9. **ImportarTipoUS** -   Listado de Tipos de US de otros proyectos para importar
10. **TUSImport** -   Importa el Tipo de US de otro proyecto
11. **validar_fases** -   valida si la cantidad de fases y las fases son correctas
12. **guardar_fases** -   Los datos validados por validar fases lo guarda en el tipo de us

"""
#Las pruebas unitarias de las vistas se encuentran en [[tests.py]]
#Los modelos se encuentran en [[models.py]]

# === RegistroTipoUS ===
class RegistrarTipoUS(PermissionRequiredMixin,CreateView):
    """

    **Hereda de:**

    1. PermissionRequiredMixin:CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. CreateView: Vista para la creacion de una nueva instancia de un objeto con una respuesta renderizada por un template

    **Retorna:** url a ListarTipoUS

    """
    model = TipoUS
    template_name = 'tipous/tipous_editar.html'
    form_class = TipousForm
    permission_required = 'proyectos.add_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        `Override:`Agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
         """
        context = super(CreateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context


    def form_valid(self, form):
        ret = super(CreateView,self).form_valid(form)
        if validar_fases(self.request):
            print ("Fases adjuntas válidas")
            guardar_fases(self.object,self.request)
        else:
            print("Fases adjuntas inválidas")
        return ret

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega al nuevo tipo de user story el indicador del proyecto
        al cual corresponde.
         """

        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.proyecto = Proyecto.objects.get(id=kwargs['pk'])
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        """
        `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        """
        log(self,ADDITION,'Nuevo TipoUS')
        l=self.object.proyecto.id
        return reverse_lazy("proyectos:tipous:listar", kwargs={'pk': l})

# === ListarTipoUS ===
class ListarTipoUS(PermissionRequiredMixin,ListView):
    """
    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. ListView: Renderiza alguna lista de objetos,seteado por  `self.model` or `self.queryset`.`self.queryset` puede ser cualquier iterable de items no solo un queryset

    **Retorna:** url a ListarTipoUS
     """
    model = TipoUS
    template_name = "tipous/tipous_listar.html"
    paginate_by = 5
    permission_required = 'proyectos.add_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_queryset(self):
        """
        `Override:` personaliza la lista de Tipo de User Story a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return TipoUS.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

# === BorrarTipoUS ===
class BorrarTipoUS(PermissionRequiredMixin,DeleteView):
    """
    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. DeleteView: Vista para borrar un objeto recolectado con  `self.get_object()`, con una respuesta renderizada con una template

    **Retorna:** url a ListarTipoUS
     """
    model = TipoUS
    template_name = 'tipous/tipous_borrar.html'
    permission_required = 'proyectos.delete_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del tipo de user story a ser eliminado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tusid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.object.proyecto.id
        log(self,DELETION,'Borrar TipoUS')
        return reverse_lazy("proyectos:tipous:listar", kwargs={'pk': l})

# === EditarTipoUS ===
class EditarTipoUS(PermissionRequiredMixin,UpdateView):
    """

    **Ancestros:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. UpdateView: Vista para modificar un objeto,con una respuesta renderizada por un template

    **Retorna**: url a ListarUsuarios
     """
    model = TipoUS
    template_name = 'tipous/tipous_editar.html'
    form_class = TipousForm
    permission_required = 'proyectos.edit_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def form_valid(self, form):
        ret = super(UpdateView,self).form_valid(form)
        if validar_fases(self.request):
            print ("Fases adjuntas válidas")
            guardar_fases(self.object,self.request)
        else:
            print("Fases adjuntas inválidas")
        return ret

    def get_context_data(self, **kwargs):
        """
        `Override:`Agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
         """
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del miembro a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tusid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.object.proyecto.id
        log(self,CHANGE,'Editar TipoUS')
        return reverse_lazy("proyectos:tipous:listar", kwargs={'pk': l})

# === RegistrarFase ===
class RegistrarFase(PermissionRequiredMixin,CreateView):
    """

    **Hereda de:**

    1. PermissionRequiredMixin:CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. CreateView: Vista para la creacion de una nueva instancia de un objeto con una respuesta renderizada por un template

    **Retorna:** url a ListarFases

     """
    model = Fase
    template_name = "tipous/tipous_registrar_fase.html"
    form_class = FaseForm
    permission_required = 'proyectos.fases_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def form_valid(self, form):
        """
        `Override:` Verifica que no se solapen el orden de fases de un tipo de user story .
        """
        try:
            self.object = form.save()
            return super(RegistrarFase, self).form_valid(form)
        except IntegrityError:
            return HttpResponse("ERROR: Ese numero de orden ya existe!")


    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de Fases de un tipo de user story,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(CreateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['tus'] = TipoUS.objects.get(id=self.kwargs['tusid'])

        return context

    def post(self, request, *args, **kwargs):
        """
        `Override:` agrega a la nueva fase el indicador del tipo de user story
        al cual esta siendo agregado.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        orden = self.request.POST.get('orden')
        print(orden)
        form.instance.tipous = TipoUS.objects.get(id=kwargs['tusid'])
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)



    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        tus = self.kwargs['tusid']
        log(self,ADDITION,'Nueva Fase')
        return reverse_lazy("proyectos:tipous:listar_fases", kwargs={'pk': pr, 'tusid': tus})

# === ListarFases ===
class ListarFases(PermissionRequiredMixin,ListView):
    """
    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. ListView: Renderiza alguna lista de objetos,seteado por  `self.model` or `self.queryset`.`self.queryset` puede ser cualquier iterable de items no solo un queryset

    **Retorna:** url a ListarFases
     """
    model = Fase
    template_name = 'tipous/tipous_listar_fase.html'
    form_class = FaseForm
    permission_required = 'proyectos.fases_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['tus'] = TipoUS.objects.get(id=self.kwargs['tusid'])
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        `Override:` personaliza la lista de fases a fin de proyectar solo los
        pertenecientes al tipo de user story.
        """
        return Fase.objects.filter(tipous=self.kwargs['tusid'])

# === BorrarFase ===
class BorrarFase(PermissionRequiredMixin,DeleteView):
    """
    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. DeleteView: Vista para borrar un objeto recolectado con  `self.get_object()`, con una respuesta renderizada con una template

    **Retorna:** url a ListarFases
     """
    model = Fase
    template_name = 'tipous/tipous_borrar_fase.html'
    permission_required = 'proyectos.fases_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación de la fase a ser eliminada.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['fid'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.

        pr = self.kwargs['pk']
        tus = self.kwargs['tusid']
        log(self,DELETION,'Borrado de Fase')

        return reverse_lazy("proyectos:tipous:listar_fases", kwargs={'pk': pr, 'tusid': tus})

# === EditarFase ===
class EditarFase(PermissionRequiredMixin,UpdateView):
    """

    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. UpdateView: Vista para modificar un objeto,con una respuesta renderizada por un template

    **Retorna**: url a ListarFases
     """
    model = Fase
    template_name = "tipous/tipous_editar_fase.html"
    form_class = FaseForm
    permission_required = 'proyectos.fases_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)


    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación de la  fase a ser modificada.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['fid'])
        obj = queryset.get()
        return obj

    def form_valid(self, form):
        """
        `Override:` Verifica que no se solapen el orden de fases de un tipo de user story .
        """

        try:
            self.object = form.save()
            return super(EditarFase, self).form_valid(form)
        except IntegrityError:
            return HttpResponse("ERROR: Ese numero de orden ya existe!")


    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        pr = self.kwargs['pk']
        tus = self.kwargs['tusid']
        log(self,CHANGE,'Edicion de la  Fase')
        return reverse_lazy("proyectos:tipous:listar_fases", kwargs={'pk': pr, 'tusid': tus})

# === ImportarTipoUS ===
class ImportarTipoUS(PermissionRequiredMixin,ListView):
    """
    **Hereda de:**

    1. PermissionRequiredMixin: CBV mixin que verifica que el usuario actual tiene todos los permisos especificados

    2. ListView: Renderiza alguna lista de objetos,seteado por  `self.model` or `self.queryset`.`self.queryset` puede ser cualquier iterable de items no solo un queryset

    **Retorna:** url a ImportarTipoUS
     """
    model = TipoUS
    template_name = "tipous/tipous_importar.html"
    paginate_by = 8
    permission_required = 'proyectos.add_Tus'
    raise_exception = True

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_queryset(self):
        """
        `Override:` personaliza la lista de Tipo de User Story a fin de desplegar los Tipos que pertenezcan a otros
         proyectos.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return TipoUS.objects.exclude(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

# === TUSImport ===
class TUSImport(PermissionRequiredMixin, TemplateView):

    template_name = "tipous/tipous_confirmar_importacion.html"
    permission_required = "proyectos.add_Tus"

    def get_permission_object(self):
        """
        `Override:` Retorna el objeto proyecto con el cual se va a verificar si el usuario cuenta permiso.
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(TemplateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['tipous'] = TipoUS.objects.get(id=self.kwargs['tusid'])
        return context


    def post(self, *args, **kwargs):
        tipous = TipoUS.objects.get(id=kwargs["tusid"])
        proyecto = Proyecto.objects.get(id=kwargs["pk"])
        tipousnuevo = TipoUS.objects.create(nombre=tipous.nombre, descripcion=tipous.descripcion, proyecto=proyecto)

        fases = Fase.objects.filter(tipous=tipous)
        for fase in fases:
            tipousnuevo.fase_set.create(nombre=fase.nombre, orden=fase.orden)

        tipousnuevo.save()
        #log(self,ADDITION,'Importacion de Tipo de US')
        return HttpResponseRedirect("/proyecto/" + str(proyecto.id) + "/tipous")

# === validar_fases ===
def validar_fases(request):
    #Funcion auxiliar para verificar que la inscripcion de TUS en `request`
    #adjunte validamente las Fases correspondientes
    """
    `Return:` True si el pedido es valido, False si no
    """
    try:
        datos = request.POST
        if not "cant_fases" in datos:
            return False
        for i in range (1,int(datos["cant_fases"])+1):
            if not  "fase_"+str(i) in datos:
                return False
    except:
        return False
    return True
# === guardar_fases ===
def guardar_fases(TUS,request):
    # Funcion auxiliar para almacenar las Fases correspondientes al `TUS`.
    """
    `Return:` True si el pedido es valido, False si no
    """
    try:
        datos = request.POST
        for i in range(1, int(datos["cant_fases"]) + 1):
            orden_act = "fase_"+str(i)
            print(orden_act)
            query= Fase.objects.filter(tipous=TUS,orden=i)
            if query.count() > 0:
                fase_act = query.get()
                if (fase_act.nombre != datos[orden_act]):
                    print("Cambio nombre de "+fase_act.nombre+" a "+datos[orden_act]+ "("+str(i)+")")
                    fase_act.nombre = datos[orden_act]
                    fase_act.save()
            else:
                Fase.objects.create(tipous=TUS,orden=i,nombre=datos[orden_act])
                print("Fase nueva: "+ datos[orden_act] + "("+str(i)+")")
        for i in Fase.objects.filter(tipous=TUS):
            if i.orden > int(datos["cant_fases"]):
                print("Fase borrada: " + i.nombre + "(" + str(i.orden) + ")")
                us_list = UserStory.objects.filter(fase_actual=None)
                i.delete()
                for us in us_list:
                    us.save()


    except Exception as e:

        print("Error en la actualizacion de fases")
        print(e)
