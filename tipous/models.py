from django.db import models
from proyectos.models import Proyecto
# === TipoUS ===
class TipoUS(models.Model):
    """
    Clase TipoUS:
    Registra un nombre,una descripcion y lo asocia a un proyecto
    """
    nombre=models.CharField(max_length=50)
    descripcion=models.CharField(max_length=150)
    proyecto=models.ForeignKey(Proyecto,on_delete=models.CASCADE)
    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering=['id']

    def __str__(self):
        return self.nombre
# === Fase ===
class Fase(models.Model):
    """
    Clase Fase:
    Registra un nombre,un orden y lo asocia a un tipo de user story
    """
    nombre=models.CharField(max_length=50)
    tipous=models.ForeignKey(TipoUS,on_delete=models.CASCADE)
    orden=models.PositiveIntegerField()

    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        unique_together = (('orden', 'tipous'),)
        ordering=['orden']

    def __str__(self):
        return self.nombre