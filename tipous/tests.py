from django.contrib.auth.models import User, Group
from django.test import TestCase

from proyectos.models import Proyecto
from tipous.models import TipoUS, Fase
from django.test import TransactionTestCase
"""
Todas las pruebas de la aplicacion tiposus
Actualmente soporta las siguientes 10 pruebas:

1. **test_registro_tipous** 
2. **test_registroinvalido_tipous** 
3. **test_editar**
4. **test_editar_invalido**
5. **test_borrar**
6. **test_registro_fase**
7. **test_registro_fase_invalido**
8. **test_borrar_fase**
9. **test_editar_fase**
10. **test_editar_fase_invalido**

"""

class test_tipous(TransactionTestCase):
    def setUp(self):
        poblar()
    def alta_developer(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/team/add"
        inscripcion = {
            "usuario": User.objects.get(username="Natalia").id,
            "rol": Group.objects.get(name="Developer").id,
            "horas_trabajo": 5,
        }
        print("Post to: " + url)
        print("Data " + str(inscripcion))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=inscripcion)
        print(respuesta.status_code)

    # === test_registro_tipous ===
    def test_registro_tipous(self):

        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/add"
        registrotipous = {
            "nombre": 'TablaPrueba',
           "descripcion":'descripcion de prueba'
        }
        print("Post to: " + url)
        print("Data " + str(registrotipous))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=registrotipous)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el registro de tipo de us")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la adicion de TablaPrueba")

    # === RegistroTipoUS invalido ===
    def test_registroinvalido_tipous(self):
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/add"
        registrotipous = {
            "nombre": 'TablaPruebainvalida',
            "descripcion": 'descripcion de prueba invalida'
        }
        print("Post to: " + url)
        print("Data " + str(registrotipous))
        self.client.login(username="Natalia", password="adminadmin")
        self.client.post(url, data=registrotipous)
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertFalse(she.count() == 1,
                         msg="Un integrante sin permisos logro añadir un tipo de user story ")
        self.client.logout()

    # === test_editar ===
    def test_editar(self):
        self.test_registro_tipous()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous=TipoUS.objects.get(proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/editar/" + str(id_tipous) + "/"
        nuevos_datos = {
            "nombre": 'TablaPruebaEditada',
            "descripcion": 'descripcion de prueba editada'
        }
        print(id_tipous)
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))

        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la actualizacion ")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la edicion de la tabla editada")

    # === test_editar_invalido ===
    def test_editar_invalido(self):
        self.test_registro_tipous()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous = TipoUS.objects.get(proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/editar/" + str(id_tipous) + "/"
        nuevos_datos = {
            "nombre": '',
            "descripcion": 'descripcion de prueba editada invalida'
        }
        print(id_tipous)
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))

        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 200, msg="Si se actualizo ")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la edicion de la tabla editada")

    # === test_borrar ===
    def test_borrar(self):
        self.test_registro_tipous()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous = TipoUS.objects.get(proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/borrar/" + str(id_tipous) + "/"
        respuesta = self.client.post(url)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el borrado")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        self.assertTrue(she.count() == 0, msg="No se ha borrado el tipo de user story")

    # === test_registro_fase ===
    def registro_fase(self):
        self.test_registro_tipous()

        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous=TipoUS.objects.get(proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/"+str(id_tipous)+"/add/"
        registrofase = {
            "nombre": 'Analisis',
            "orden": '1'
        }
        print("Post to: " + url)
        print("Data " + str(registrofase))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=registrofase)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el registro de la fase")
        she = Fase.objects.filter(tipous=id_tipous)
        self.assertTrue(she.count() == 1, msg="No se ha almacenado la adicion de la fase Analisis")

    # === test_registro_fase_invalido ===
    def registro_fase_invalido(self):
        self.test_registro_fase()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous = TipoUS.objects.get(proyecto_id=id_proyecto).id
        url = "/proyecto/" + str(id_proyecto) + "/tipous/" + str(id_tipous) + "/add/"
        registrofase = {
            "nombre": 'Analisis',
            "orden": '1'
        }

        print("Post to: " + url)
        print("Data " + str(registrofase))
        respuesta = self.client.post(url, data=registrofase)
        print(respuesta.status_code)
        self.assertFalse(respuesta.status_code == 302, msg="Error en el registro de la fase")
        she = TipoUS.objects.filter(proyecto_id=id_proyecto)
        print(she)
        self.assertFalse(she.count() == 2, msg="Se ha almacenado la adicion de la fase Analisis")
        self.client.logout()
        self.client.login(username="Natalia", password="adminadmin")
        self.client.post(url, data=registrofase)
        she = Fase.objects.filter(tipous=id_tipous)
        self.assertFalse(she.count() == 2,
                         msg="Una integrante sin permisos logro añadir una fase ")

    # === test_borrar_fase ===
    def borrar_fase(self):
        self.test_registro_fase()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous = TipoUS.objects.get(proyecto_id=id_proyecto,nombre='TablaPrueba').id
        id_fase=Fase.objects.get(tipous=id_tipous,nombre='Analisis').id
        print("id_fase",id_fase)
        url = "/proyecto/" + str(id_proyecto) + "/tipous/"+ str(id_tipous) +"/borrar/" + str(id_fase)
        print("Post to: " + url)
        respuesta = self.client.post(url)
        print(respuesta.status_code)
        self.assertTrue(respuesta.status_code == 302, msg="Error en el borrado")
        she = Fase.objects.filter(tipous=id_tipous)
        self.assertTrue(she.count() == 0, msg="No se ha borrado de la fase")

    # === test_editar_fase ===
    def editar_fase(self):
        self.test_registro_fase()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous=TipoUS.objects.get(proyecto_id=id_proyecto,nombre='TablaPrueba').id
        id_fase = Fase.objects.get(tipous=id_tipous, nombre='Analisis').id
        print('Las fases',Fase.objects.filter(tipous=id_tipous, nombre='Analisis'))
        print("id_fase", id_fase)
        url = "/proyecto/" + str(id_proyecto) + "/tipous/" + str(id_tipous) + "/add/"
        registrofase = {
            "nombre": 'Desarrollo',
            "orden": '2'
        }
        print("Post to: " + url)
        print("Data " + str(registrofase))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=registrofase)

        url = "/proyecto/" + str(id_proyecto) + "/tipous/" + str(id_tipous) + "/editar/" + str(id_fase)
        nuevos_datos = {
            "nombre": 'Analisis',
            "orden": '3'
        }
        print('id_tipous',id_tipous)
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))

        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        print('Las fases', Fase.objects.get(tipous=id_tipous, nombre='Analisis').orden)
        self.assertTrue(respuesta.status_code == 302, msg="Error en la actualizacion ")
        she = Fase.objects.filter(tipous=id_tipous)
        self.assertTrue(she.count() == 2, msg="No se ha almacenado la edicion de la fase editada")

    # === test_editar_fase_invalido ===
    def editar_fase_invalido(self):
        self.test_registro_fase()
        id_proyecto = Proyecto.objects.get(nombre="SGP").id
        id_tipous=TipoUS.objects.get(proyecto_id=id_proyecto,nombre='TablaPrueba').id
        id_fase = Fase.objects.get(tipous=id_tipous, nombre='Analisis').id
        print('Las fases',Fase.objects.filter(tipous=id_tipous, nombre='Analisis'))
        print("id_fase", id_fase)
        url = "/proyecto/" + str(id_proyecto) + "/tipous/" + str(id_tipous) + "/add/"
        registrofase = {
            "nombre": 'Desarrollo Editar invalido',
            "orden": '2'
        }
        print("Post to: " + url)
        print("Data " + str(registrofase))
        self.client.login(username="Eduardo", password="adminadmin")
        respuesta = self.client.post(url, data=registrofase)

        url = "/proyecto/" + str(id_proyecto) + "/tipous/" + str(id_tipous) + "/editar/" + str(id_fase)
        nuevos_datos = {
            "nombre": 'Analisis',
            "orden": '2'
        }
        print('id_tipous',id_tipous)
        print("Post to: " + url)
        print("Data " + str(nuevos_datos))

        respuesta = self.client.post(url, data=nuevos_datos)
        print(respuesta.status_code)
        print('Las fases', Fase.objects.get(tipous=id_tipous, nombre='Analisis').orden)
        self.assertTrue(respuesta.status_code == 200, msg="Si se actualizo ")
        she = Fase.objects.filter(tipous=id_tipous)
        self.assertTrue(she.count() == 2, msg="No se ha almacenado la edicion de la fase editada")




from django.contrib.auth.models import User, Permission, Group
from team.models import team_member
from proyectos.models import Proyecto

def poblar():
    perm_scrum=['add_Integrante','edit_Integrante','delete_Integrante',
                'view_proyecto','change_proyecto','delete_proyecto','add_Tus','edit_Tus','delete_Tus','fases_Tus']

    usuarios=["Alan","Eduardo","Guillermo","Natalia"]
    roles=["Scrum Master","Developer","Tester"]
    proyectos=["SGP","ATM","Download Tester"]
    new_users = {}
    new_roles = {}
    new_proyectos = {}
    new_sys_roles= {}
    for i in ["Administrador","Usuario Normal"]:
        query=Group.objects.filter(name=i)
        if query.count()>0:
            new_sys_roles[i]=query.get()
        else:
            new_sys_roles[i]=Group.objects.create(name=i)
    new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename='view_proyecto'))
    new_sys_roles["Usuario Normal"].permissions.add(Permission.objects.get(codename='add_proyecto'))
    for i in ["add","change","delete"]:
        for j in ["group","user"]:
            new_sys_roles["Administrador"].permissions.add(Permission.objects.get(codename=i+'_'+j))

    for i in usuarios:
        try:
            query= User.objects.filter(username=i)
            if query.count()==1 :
                new_users[i]=query.get()
            else:
                new_users[i]=User.objects.create_user(username=i,password="adminadmin")
        except:
            if User.objects.filter(username=i).count()==0:
                print("No se puedo crear el usuario :"+i)
        if i == "Guillermo":
            new_users[i].groups.add(new_sys_roles["Administrador"])
        else:
            new_users[i].groups.add(new_sys_roles["Usuario Normal"])
    for i in roles:
        try:
            query=Group.objects.filter(name=i)
            if query.count()==1:
                new_roles[i]=query.get()
            else:
                new_roles[i]=Group.objects.create(name=i)
        except:
            print("No se pudo crear rol")

    for i in perm_scrum:
        try:
            grupo = Group.objects.get(name="Scrum Master")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

    for i in proyectos:
            if Proyecto.objects.filter(nombre=i).count() == 0 :
                new_proyectos[i]=Proyecto.objects.create(nombre=i,descripcion="")
                team_member.objects.create(usuario=new_users["Eduardo"],
                                           rol=new_roles["Scrum Master"],
                                           proyecto=new_proyectos[i])

    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="add_group"))
    User.objects.get(username="Eduardo").user_permissions.add(Permission.objects.get(codename="change_group"))


